namespace rupbes.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Configuration;

    public partial class Database : DbContext
    {
        static ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["Database"];
        static string connStr = settings.ConnectionString.Insert(settings.ConnectionString.Length - 1, ";password=Qwerty123!");
        public Database()
            : base(connStr)
        {
        }

        public virtual DbSet<Bosses> Bosses { get; set; }
        public virtual DbSet<Certificates> Certificates { get; set; }
        public virtual DbSet<Contacts> Contacts { get; set; }
        public virtual DbSet<Dep_groups> Dep_groups { get; set; }
        public virtual DbSet<Dep_types> Dep_types { get; set; }
        public virtual DbSet<Departments> Departments { get; set; }
        public virtual DbSet<Deps_to_groups> Deps_to_groups { get; set; }
        public virtual DbSet<Img_types> Img_types { get; set; }
        public virtual DbSet<Imgs> Imgs { get; set; }
        public virtual DbSet<Mechanisms> Mechanisms { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<News_type> News_type { get; set; }
        public virtual DbSet<Objects> Objects { get; set; }
        public virtual DbSet<Realty> Realty { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<Services> Services { get; set; }
        public virtual DbSet<Usage_report> Usage_report { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<Vacancies> Vacancies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Certificates>()
                .HasMany(e => e.Imgs)
                .WithMany(e => e.Certificates)
                .Map(m => m.ToTable("Imgs_to_cerificate", "rupbesby_admin").MapLeftKey("id_certificate").MapRightKey("id_img"));

            modelBuilder.Entity<Dep_groups>()
                .HasMany(e => e.Deps_to_groups)
                .WithRequired(e => e.Dep_groups)
                .HasForeignKey(e => e.id_group);

            modelBuilder.Entity<Dep_types>()
                .HasMany(e => e.Departments)
                .WithRequired(e => e.Dep_types)
                .HasForeignKey(e => e.type_id);

            modelBuilder.Entity<Departments>()
                .HasMany(e => e.Bosses)
                .WithRequired(e => e.Departments)
                .HasForeignKey(e => e.id_dep);

            modelBuilder.Entity<Departments>()
                .HasMany(e => e.Certificates)
                .WithRequired(e => e.Departments)
                .HasForeignKey(e => e.id_dep)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Departments>()
                .HasMany(e => e.Deps_to_groups)
                .WithRequired(e => e.Departments)
                .HasForeignKey(e => e.id_dep);

            modelBuilder.Entity<Departments>()
                .HasMany(e => e.Mechanisms)
                .WithRequired(e => e.Departments)
                .HasForeignKey(e => e.id_dep);

            modelBuilder.Entity<Departments>()
                .HasMany(e => e.News)
                .WithRequired(e => e.Departments)
                .HasForeignKey(e => e.id_dep)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Departments>()
                .HasMany(e => e.Realty)
                .WithRequired(e => e.Departments)
                .HasForeignKey(e => e.id_dep);

            modelBuilder.Entity<Departments>()
                .HasMany(e => e.Services)
                .WithRequired(e => e.Departments)
                .HasForeignKey(e => e.id_dep);

            modelBuilder.Entity<Departments>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.Departments)
                .HasForeignKey(e => e.id_dep);

            modelBuilder.Entity<Departments>()
                .HasMany(e => e.Vacancies)
                .WithRequired(e => e.Departments)
                .HasForeignKey(e => e.id_dep);

            modelBuilder.Entity<Departments>()
                .HasMany(e => e.Objects)
                .WithMany(e => e.Departments)
                .Map(m => m.ToTable("Deps_to_objects", "rupbesby_admin").MapLeftKey("id_dep").MapRightKey("id_obj"));

            modelBuilder.Entity<Deps_to_groups>()
                .HasOptional(e => e.Contacts)
                .WithRequired(e => e.Deps_to_groups)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Img_types>()
                .HasMany(e => e.Imgs)
                .WithOptional(e => e.Img_types)
                .HasForeignKey(e => e.type_id)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Imgs>()
                .HasMany(e => e.Bosses)
                .WithRequired(e => e.Imgs)
                .HasForeignKey(e => e.id_img)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Imgs>()
                .HasMany(e => e.Departments)
                .WithOptional(e => e.Imgs)
                .HasForeignKey(e => e.id_img)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Imgs>()
                .HasMany(e => e.Mechanisms)
                .WithMany(e => e.Imgs)
                .Map(m => m.ToTable("Imgs_to_mechanisms", "rupbesby_admin").MapLeftKey("id_img").MapRightKey("id_mech"));

            modelBuilder.Entity<Imgs>()
                .HasMany(e => e.News)
                .WithMany(e => e.Imgs)
                .Map(m => m.ToTable("Imgs_to_news", "rupbesby_admin").MapLeftKey("id_img").MapRightKey("id_news"));

            modelBuilder.Entity<Imgs>()
                .HasMany(e => e.Objects)
                .WithMany(e => e.Imgs)
                .Map(m => m.ToTable("Imgs_to_objects", "rupbesby_admin").MapLeftKey("id_img").MapRightKey("id_object"));

            modelBuilder.Entity<Imgs>()
                .HasMany(e => e.Realty)
                .WithMany(e => e.Imgs)
                .Map(m => m.ToTable("Imgs_to_realty", "rupbesby_admin").MapLeftKey("id_img").MapRightKey("id_realty"));

            modelBuilder.Entity<Imgs>()
                .HasMany(e => e.Services)
                .WithMany(e => e.Imgs)
                .Map(m => m.ToTable("Imgs_to_services", "rupbesby_admin").MapLeftKey("id_img").MapRightKey("id_service"));

            modelBuilder.Entity<News>()
                .Property(e => e.body_ru)
                .IsUnicode(false);

            modelBuilder.Entity<News>()
                .Property(e => e.body_bel)
                .IsUnicode(false);

            modelBuilder.Entity<News_type>()
                .HasMany(e => e.News)
                .WithRequired(e => e.News_type)
                .HasForeignKey(e => e.type_id);

            modelBuilder.Entity<Objects>()
                .Property(e => e.desc_ru)
                .IsUnicode(false);

            modelBuilder.Entity<Objects>()
                .Property(e => e.desc_bel)
                .IsUnicode(false);

            modelBuilder.Entity<Roles>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.Roles)
                .HasForeignKey(e => e.id_role);

            modelBuilder.Entity<Users>()
                .HasMany(e => e.Usage_report)
                .WithRequired(e => e.Users)
                .HasForeignKey(e => e.id_user);
        }
    }
}
