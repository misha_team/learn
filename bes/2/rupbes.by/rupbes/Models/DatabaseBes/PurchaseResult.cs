﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace rupbes.Models.DatabaseBes
{
    public class PurchaseResult
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PurchaseResult()
        {
            Purchases = new HashSet<Purchases>();
        }
        public int Id { get; set; }
        public string Result { get; set; } //результат закупки

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Purchases> Purchases { get; set; } //связь с таблицей закупок
    }
}