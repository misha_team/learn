﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace rupbes.Models.DatabaseBes
{
    public class Purchases
    {
        public int Id { get; set; }             // номер
        [DataType(DataType.Date)]
        public DateTime? DateReg { get; set; }      // дата регистрации
        public int NumberTT { get; set; }       // номер тех задания
        public string Content { get; set; }     // содержание/наименование кратко
        public string TTLink { get; set; }      // ссылка на техзадание
        [DataType(DataType.Date)]
        public DateTime? DateContract { get; set; } // дата закрытия договора
        public string ContractNumber { get; set; } // номер договора
        public string ClosingNote { get; set; }    // примечание при закрытии сделки
        public string DealAmount { get; set; }    // сумма договора

        //связь с таблицей видов процедуры закупки
        public int TypeOfContractId { get; set; }
        public virtual TypeOfContract Type { get; set; }

        //связь с таблицей типов валют
        public int? CurrencyTypeId { get; set; }
        public virtual CurrencyType CurrencyType { get; set; }

        //связь с таблицей типов результатов закупки
        public int? PurchaseResultId { get; set; }
        public virtual PurchaseResult PurchaseResult { get; set; }

        //для кого
        public int? CustomerId { get; set; }
        public virtual Company Customer { get; set; }
        //заказчик
        public int? OrganizerId { get; set; }
        public virtual Company Organizer { get; set; }
        //поставщик
        public int? ProviderId { get; set; }
        public virtual Company Provider { get; set; }
    }
}