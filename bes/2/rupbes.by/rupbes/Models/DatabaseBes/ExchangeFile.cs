﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace rupbes.Models.DatabaseBes
{
    public class ExchangeFile
    {
        [Key]
        public int Id { get; set; }
        public string FileName { get; set; }
        public DateTime Date { get; set; }

        public virtual List<Bid> Bids { get; set; }
    }
}
