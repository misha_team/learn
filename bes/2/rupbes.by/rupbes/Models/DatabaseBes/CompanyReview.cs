﻿using System.ComponentModel.DataAnnotations;

namespace rupbes.Models.DatabaseBes
{
    public partial class CompanyReview
    {
        [Key]
        public int Id { get; set; }

        public int IdCompany { get; set; }

        public int IdCompanyThatMadeReview { get; set; }

        public int IdDateReview { get; set; }

        public bool Confirm { get; set; }

        public string ReviewText { get; set; }


        public virtual Company Company { get; set; }
        public virtual CompanyThatMadeReview CompanyThatMadeReview { get; set; }
        public virtual Date DateReview { get; set; }
    }
}