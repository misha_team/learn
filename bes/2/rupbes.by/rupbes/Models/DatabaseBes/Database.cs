using System.Data.Entity;
using System.Configuration;

namespace rupbes.Models.DatabaseBes
{   
    public partial class Database : DbContext
    {

        static ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["DatabaseBes"];
        static string connStr = settings.ConnectionString.Insert(settings.ConnectionString.Length - 1, ";password=Qwerty123!");
        public Database()
            : base(connStr)
        {
        }

        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<Tenders> Tenders { get; set; }
        public virtual DbSet<Purchases> Purchases { get; set; }
        public virtual DbSet<ExchangeFile> ExchangeFiles { get; set; }
        public virtual DbSet<Bid> Bids { get; set; }

        public virtual DbSet<CompanyReview> CompanyReviews { get; set; }
        public virtual DbSet<CompanyThatMadeReview> CompanyThatMadeReviews { get; set; }
        public virtual DbSet<Date> Dates { get; set; }

        public virtual DbSet<Document> Documents { get; set; }

        public virtual DbSet<Besi_price> Besi_prices { get; set; }
        public virtual DbSet<Besi_price_list> Besi_price_lists { get; set; }
        public virtual DbSet<Besi_type_product> Besi_type_products { get; set; }
        public virtual DbSet<Besi_unit> Besi_units { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //�����������
            modelBuilder.Entity<Company>().ToTable("p000006");
            modelBuilder.Entity<Company>().Property(p => p.Id).HasColumnName("code");
            modelBuilder.Entity<Company>().Property(p => p.Name).HasColumnName("name");
            modelBuilder.Entity<Company>().Property(p => p.Abbreviation).HasColumnName("abbreviation");
            modelBuilder.Entity<Company>().Property(p => p.Unn).HasColumnName("unn");
            modelBuilder.Entity<Company>().Property(p => p.Okpo).HasColumnName("okpo");
            //����� �����������
            modelBuilder.Entity<Company>()
                .HasMany(e => e.Departments)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.CompanId);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.CompanyReviews)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.IdCompany);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Contacts)
                .WithMany(e => e.Companies)
                .Map(m => m.ToTable("s000012").MapLeftKey("code_p000006").MapRightKey("code_p000015"));
            //�����
            modelBuilder.Entity<Department>().ToTable("p000007");
            modelBuilder.Entity<Department>().Property(p => p.Id).HasColumnName("code");
            modelBuilder.Entity<Department>().Property(p => p.CompanId).HasColumnName("code_p000006");
            modelBuilder.Entity<Department>().Property(p => p.Name).HasColumnName("name");
            //����� ������
            modelBuilder.Entity<Department>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.Department)
                .HasForeignKey(e => e.DepartmentId);

            modelBuilder.Entity<Department>()
                .HasMany(e => e.Contacts)
                .WithMany(e => e.Departments)
                .Map(m => m.ToTable("s000013").MapLeftKey("code_p000007").MapRightKey("code_p000015"));
            //���������
            modelBuilder.Entity<Employee>().ToTable("p000009");
            modelBuilder.Entity<Employee>().Property(p => p.Id).HasColumnName("code");
            modelBuilder.Entity<Employee>().Property(p => p.PostId).HasColumnName("code_p000008");
            modelBuilder.Entity<Employee>().Property(p => p.DepartmentId).HasColumnName("code_p000007");
            modelBuilder.Entity<Employee>().Property(p => p.PersonId).HasColumnName("code_p000011");
            modelBuilder.Entity<Employee>().Property(p => p.TabelNumber).HasColumnName("num_tab");
            modelBuilder.Entity<Employee>().Property(p => p.PensionNumber).HasColumnName("num_sertificate_pens");
            modelBuilder.Entity<Employee>().Property(p => p.InsuranceNumber).HasColumnName("num_sertificate_insurance");
            //����� ����������
            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Contacts)
                .WithMany(e => e.Employees)
                .Map(m => m.ToTable("s000016").MapLeftKey("code_p000009").MapRightKey("code_p000015"));
            //���������
            modelBuilder.Entity<Post>().ToTable("p000008");
            modelBuilder.Entity<Post>().Property(p => p.Id).HasColumnName("code");
            modelBuilder.Entity<Post>().Property(p => p.Name).HasColumnName("name");
            //����� ���������
            modelBuilder.Entity<Post>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.Post)
                .HasForeignKey(e => e.PostId);
            //���������� ����
            modelBuilder.Entity<Person>().ToTable("p000011");
            modelBuilder.Entity<Person>().Property(p => p.Id).HasColumnName("code");
            modelBuilder.Entity<Person>().Property(p => p.FirstName).HasColumnName("first_name");
            modelBuilder.Entity<Person>().Property(p => p.LastName).HasColumnName("last_name");
            modelBuilder.Entity<Person>().Property(p => p.FatherName).HasColumnName("father_name");
            //����� ��� ����
            modelBuilder.Entity<Person>()
                .HasMany(e => e.Contacts)
                .WithMany(e => e.Persons)
                .Map(m => m.ToTable("s000014").MapLeftKey("code_p000011").MapRightKey("code_p000015"));
            modelBuilder.Entity<Person>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.Person)
                .HasForeignKey(e => e.PersonId);

            //�������
            modelBuilder.Entity<Contact>().ToTable("p000015");
            modelBuilder.Entity<Contact>().Property(p => p.Id).HasColumnName("code");
            modelBuilder.Entity<Contact>().Property(p => p.ContactsTypeId).HasColumnName("code_p000010");
            modelBuilder.Entity<Contact>().Property(p => p.ContactName).HasColumnName("contact");
            //���� ���������
            modelBuilder.Entity<ContactsType>().ToTable("p000010");
            modelBuilder.Entity<ContactsType>().Property(p => p.Id).HasColumnName("code");
            modelBuilder.Entity<ContactsType>().Property(p => p.Type).HasColumnName("contact_type");
            //����� ���� ���������
            modelBuilder.Entity<ContactsType>()
                .HasMany(e => e.Contacts)
                .WithRequired(e => e.ContactsType)
                .HasForeignKey(e => e.ContactsTypeId);

            //�������
            modelBuilder.Entity<Tenders>().ToTable("p000033");

            //�������
            modelBuilder.Entity<Purchases>().ToTable("p000034");
            modelBuilder.Entity<PurchaseResult>().ToTable("p000036");
            modelBuilder.Entity<CurrencyType>().ToTable("p000037");
            modelBuilder.Entity<TypeOfContract>().ToTable("p000035");

            //�����
            modelBuilder.Entity<Bid>().ToTable("p000054");
            modelBuilder.Entity<ExchangeFile>().ToTable("p000055");

            //����� �� �����������
            modelBuilder.Entity<CompanyReview>().ToTable("p000039");
            modelBuilder.Entity<CompanyReview>().Property(p => p.Id).HasColumnName("code");
            modelBuilder.Entity<CompanyReview>().Property(p => p.IdCompany).HasColumnName("code_p000006");
            modelBuilder.Entity<CompanyReview>().Property(p => p.IdCompanyThatMadeReview).HasColumnName("code_p000040");
            modelBuilder.Entity<CompanyReview>().Property(p => p.IdDateReview).HasColumnName("code_date_review");
            modelBuilder.Entity<CompanyReview>().Property(p => p.Confirm).HasColumnName("confirm");
            modelBuilder.Entity<CompanyReview>().Property(p => p.ReviewText).HasColumnName("review_text");

            //�����������, ���������� �����
            modelBuilder.Entity<CompanyThatMadeReview>().ToTable("p000040");
            modelBuilder.Entity<CompanyThatMadeReview>().Property(p => p.Id).HasColumnName("code");
            modelBuilder.Entity<CompanyThatMadeReview>().Property(p => p.Name).HasColumnName("name");
            modelBuilder.Entity<CompanyThatMadeReview>().Property(p => p.Email).HasColumnName("email");
            //����� �����������, ���������� �����
            modelBuilder.Entity<CompanyThatMadeReview>()
                .HasMany(e => e.CompanyReviews)
                .WithRequired(e => e.CompanyThatMadeReview)
                .HasForeignKey(e => e.IdCompanyThatMadeReview);

            // ����
            modelBuilder.Entity<Date>().ToTable("p000061");
            modelBuilder.Entity<Date>().Property(p => p.Id).HasColumnName("code");
            modelBuilder.Entity<Date>().Property(p => p.DateValue).HasColumnName("date");
            // ����� ������� "����"
            modelBuilder.Entity<Date>()
                .HasMany(e => e.CompanyReviews)
                .WithRequired(e => e.DateReview)
                .HasForeignKey(e => e.IdDateReview);

            modelBuilder.Entity<Date>()
                .HasMany(e => e.Documents)
                .WithRequired(e => e.DateDocument)
                .HasForeignKey(e => e.IdDateDocument);

            modelBuilder.Entity<Date>()
                .HasMany(e => e.Besi_Prices)
                .WithRequired(e => e.DatePrice)
                .HasForeignKey(e => e.IdDatePrice);

            //���������
            modelBuilder.Entity<Document>().ToTable("p000067");
            modelBuilder.Entity<Document>().Property(o => o.Id).HasColumnName("code");
            modelBuilder.Entity<Document>().Property(o => o.Link).HasColumnName("link");
            modelBuilder.Entity<Document>().Property(o => o.NumberDocument).HasColumnName("number_document");
            modelBuilder.Entity<Document>().Property(o => o.IdDateDocument).HasColumnName("code_date");

            modelBuilder.Entity<Document>()
                .HasMany(e => e.Besi_Type_Products_Photo)
                .WithRequired(e => e.Photo)
                .HasForeignKey(e => e.IdPhoto);

            modelBuilder.Entity<Document>()
                .HasMany(e => e.Besi_Type_Products_Drawing)
                .WithRequired(e => e.Drawing)
                .HasForeignKey(e => e.IdDrawing);

            //�������� ����
            modelBuilder.Entity<Besi_unit>().ToTable("p000092");
            modelBuilder.Entity<Besi_unit>().Property(o => o.Id).HasColumnName("code");
            modelBuilder.Entity<Besi_unit>().Property(o => o.Name).HasColumnName("name");

            modelBuilder.Entity<Besi_unit>()
                .HasMany(e => e.Besi_Price_Lists)
                .WithRequired(e => e.Besi_unit)
                .HasForeignKey(e => e.IdBesi_unit);

            modelBuilder.Entity<Besi_type_product>().ToTable("p000091");
            modelBuilder.Entity<Besi_type_product>().Property(o => o.Id).HasColumnName("code");
            modelBuilder.Entity<Besi_type_product>().Property(o => o.Name).HasColumnName("name");
            modelBuilder.Entity<Besi_type_product>().Property(o => o.IdPhoto).HasColumnName("doc_photo");
            modelBuilder.Entity<Besi_type_product>().Property(o => o.IdDrawing).HasColumnName("doc_drawing");

            modelBuilder.Entity<Besi_type_product>()
                .HasMany(e => e.Besi_Price_Lists)
                .WithRequired(e => e.Besi_type_product)
                .HasForeignKey(e => e.IdBesi_type_product);

            modelBuilder.Entity<Besi_price>().ToTable("p000089");
            modelBuilder.Entity<Besi_price>().Property(o => o.Id).HasColumnName("code");
            modelBuilder.Entity<Besi_price>().Property(o => o.Price).HasColumnName("price");
            modelBuilder.Entity<Besi_price>().Property(o => o.Price_w_nds).HasColumnName("price_w_nds");
            modelBuilder.Entity<Besi_price>().Property(o => o.IdDatePrice).HasColumnName("code_p000061");
            modelBuilder.Entity<Besi_price>().Property(o => o.IdBesi_price_list).HasColumnName("code_p000090");

            modelBuilder.Entity<Besi_price_list>().ToTable("p000090");
            modelBuilder.Entity<Besi_price_list>().Property(o => o.Id).HasColumnName("code");
            modelBuilder.Entity<Besi_price_list>().Property(o => o.Number).HasColumnName("number");
            modelBuilder.Entity<Besi_price_list>().Property(o => o.Name).HasColumnName("name");
            modelBuilder.Entity<Besi_price_list>().Property(o => o.Volume).HasColumnName("volume");
            modelBuilder.Entity<Besi_price_list>().Property(o => o.Weight).HasColumnName("weight");
            modelBuilder.Entity<Besi_price_list>().Property(o => o.IdBesi_unit).HasColumnName("code_p000092");
            modelBuilder.Entity<Besi_price_list>().Property(o => o.IdBesi_type_product).HasColumnName("code_p000091");

            modelBuilder.Entity<Besi_price_list>()
                .HasMany(e => e.Besi_Prices)
                .WithRequired(e => e.Besi_price_list)
                .HasForeignKey(e => e.IdBesi_price_list);
        }

        public System.Data.Entity.DbSet<rupbes.Models.DatabaseBes.CurrencyType> CurrencyTypes { get; set; }

        public System.Data.Entity.DbSet<rupbes.Models.DatabaseBes.PurchaseResult> PurchaseResults { get; set; }

        public System.Data.Entity.DbSet<rupbes.Models.DatabaseBes.TypeOfContract> TypeOfContracts { get; set; }

        public System.Data.Entity.DbSet<rupbes.Models.DatabaseBes.ContactsType> ContactsTypes { get; set; }
    }
}
