﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace rupbes.Models.DatabaseBes
{
    public class TypeOfContract
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TypeOfContract()
        {
            Purchase = new HashSet<Purchases>();
        }
        public int Id { get; set; }         //номер
        public string Type { get; set; }    //вид процедуры закупки

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Purchases> Purchase { get; set; }         //связь с таблицей закупок
    }
}