﻿using rupbes.Classes;

namespace rupbes.Models.DatabaseBes
{
    public class PurchasesViewModel
    {
        public PaginatedList<Tenders> TendersList { get; set; }
        public PaginatedList<Purchases> PurchasesList { get; set; }
        public PaginatedList<Bid> BidsList { get; set; }
    }
}