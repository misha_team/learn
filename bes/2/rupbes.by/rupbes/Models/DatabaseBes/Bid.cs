﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace rupbes.Models.DatabaseBes
{
    public class Bid
    {
        [Key]
        public int Id { get; set; }
        public string NameOfProduct { get; set; }
        public string Standart { get; set; }
        public string Mark { get; set; }
        public string StandartSize { get; set; }
        public string Amount { get; set; }
        public string Units { get; set; }
        public string Sum { get; set; }
        public string DeliveryTime { get; set; }
        public string DeliveryConditions { get; set; }
        public string PayConditions { get; set; }
        public string Buy { get; set; }
        public string AdditionalInf { get; set; }
        public string StructSubdivision { get; set; }

        [ForeignKey(nameof(ExchangeFile))]
        public int ExchangeFile_Id { get; set; }
        public virtual ExchangeFile ExchangeFile { get; set; }
    }
}