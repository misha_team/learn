﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace rupbes.Models.DatabaseBes
{
    public partial class Date
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Date()
        {
            CompanyReviews = new HashSet<CompanyReview>();
            Documents = new HashSet<Document>();
            Besi_Prices = new HashSet<Besi_price>();
        }

        [Key]
        public int Id { get; set; }

        public DateTime DateValue { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanyReview> CompanyReviews { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Document> Documents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Besi_price> Besi_Prices { get; set; }
    }
}