﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using rupbes.Models.DatabaseBes;

namespace rupbes.Controllers
{
    [Filters.Culture]
    public class DirectoryController : Controller
    {
        private Database db = new Database();

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Companies = db.Companies.Where(x => (x.Id >= 9) && (x.Id <= 17)).ToList();
            return View();
        }

        [HttpPost]
        public ActionResult SearchName(string searchStr)
        {

            List<Employee> employees = db.Employees.Where(x =>String.Concat(
                x.Person.LastName, " ", x.Person.FirstName, " ", x.Person.FatherName, " ",
                x.Person.FirstName, " ", x.Person.LastName, " ", x.Person.FatherName, " ",
                x.Person.LastName, " ", x.Person.FatherName, " ", x.Person.FirstName, " ",
                x.Person.FirstName, " ", x.Person.FatherName, " ", x.Person.LastName, " ",
                x.Person.FatherName, " ", x.Person.LastName, " ", x.Person.FirstName, " ",
                x.Person.FatherName, " ", x.Person.FirstName, " ", x.Person.LastName)
                .Contains(searchStr)).ToList();

            if (employees.Count > 0)
            {
                return PartialView("_SearchName", employees);
            }
            else
            {
                string result = "По вашему запросу ничего не найдено";
                return PartialView("_SearchEmpty", result);
            }
        }

        [HttpPost]
        public ActionResult SelectCompany(int companyId)
        {
            ViewBag.Departments = db.Departments.Where(x => x.CompanId == companyId);
            Company company = db.Companies.Find(companyId);
            return PartialView("_SelectCompany",company);
        }
        [HttpPost]
        public ActionResult SelectDepartment(int depId)
        {
            List<Employee> employees = db.Employees.Where(x => x.DepartmentId == depId).ToList();
            ViewBag.Department = db.Departments.Find(depId);
            return PartialView("_SelectDepartment", employees);
        }
    }
}