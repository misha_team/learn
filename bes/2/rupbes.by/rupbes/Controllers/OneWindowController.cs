﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Mail;
using BotDetect.Web.Mvc;
using rupbes.Models;

namespace rupbes.Controllers
{
    [Filters.Culture]
    public class OneWindowController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            HttpCookie cookie = Request.Cookies["lang"];
            if (cookie != null && cookie.Value == "be")
            {
                return View("IndexBel");
            }
            else
            {
                return View();
            }
                
        }

        [HttpGet]
        public ActionResult Person(string message=" ")
        {
            HttpCookie cookie = Request.Cookies["lang"];
            ViewBag.FileMessage = "";
            if (message == "Error")
            {
                if (cookie != null && cookie.Value == "be")
                {
                    ViewBag.FileMessage = "Занадта вялiкi файл";
                }
                else
                {
                    ViewBag.FileMessage = "Слишком большой файл";
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult Legal(string message = " ")
        {
            HttpCookie cookie = Request.Cookies["lang"];
            ViewBag.FileMessage = "";
            if (message == "Error")
            {
                if (cookie != null && cookie.Value == "be")
                {
                    ViewBag.FileMessage = "Занадта вялiкi файл";
                }
                else
                {
                    ViewBag.FileMessage = "Слишком большой файл";
                }
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CaptchaValidation("CaptchaCode", "Captcha", "Incorrect CAPTCHA code!")]
        public ActionResult Person(PersonMessage person, HttpPostedFileBase upload=null)
        {
            ViewBag.FileMessage = "";
            ViewBag.CaptchaMessage = "";
            HttpCookie cookie = Request.Cookies["lang"];
            if (!ModelState.IsValid)
            {
                //captcha or model validation failed
                if(cookie!=null && cookie.Value == "be")
                {
                    ViewBag.CaptchaMessage = "Увядзіце правільныя сімвалы";
                }
                else
                {
                    ViewBag.CaptchaMessage = "Введите правильные символы";
                }
                return View();
            }
            else
            {
                MvcCaptcha.ResetCaptcha("Captcha");
                //captcha and model validation passed
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress("appeal.rupbes@yandex.by");
                    mail.To.Add(new MailAddress("rupbes@rupbes.by"));
                    mail.Subject = person.Theme;
                    if (upload != null)
                    {
                        Attachment file = new Attachment(upload.InputStream, upload.FileName);
                        mail.Attachments.Add(file);
                    }
                    
                    mail.Body = " Обратившееся лицо: \r\n" + person.Sender + "\r\n Адресс лица: \r\n" + person.Adress + "\r\n" + person.Email + "\r\n Обратился к: \r\n" + person.Reciver + "\r\n Текст обращения: \r\n" + person.Text;
                    using (SmtpClient client = new SmtpClient())
                    {
                        client.Host = "smtp.yandex.ru";
                        client.Port = 587;
                        client.EnableSsl = true;
                        client.Credentials = new NetworkCredential("appeal.rupbes@yandex.by", "Qwerty123!");
                        try
                        {
                            client.Send(mail);
                            return View("MessageSent");
                        }
                        catch(Exception)
                        {
                            ViewBag.FileMessage = "Ошибка отправки обращения";
                            return View();
                        }

                    }

                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CaptchaValidation("CaptchaCode", "Captcha", "Incorrect CAPTCHA code!")]
        public ActionResult Legal(LegalMessage legal, HttpPostedFileBase upload = null)
        {
            ViewBag.FileMessage = "";
            ViewBag.CaptchaMessage = "";
            HttpCookie cookie = Request.Cookies["lang"];
            if (!ModelState.IsValid)
            {
                //captcha or model validation failed
                if (cookie != null && cookie.Value == "be")
                {
                    ViewBag.CaptchaMessage = "Увядзіце правільныя сімвалы";
                }
                else
                {
                    ViewBag.CaptchaMessage = "Введите правильные символы";
                }
                return View();
            }
            else
            {
                MvcCaptcha.ResetCaptcha("Captcha");
                //captcha and model validation passed
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress("appeal.rupbes@yandex.by");
                    mail.To.Add(new MailAddress("rupbes@rupbes.by"));
                    mail.Subject = legal.Theme;
                    if (upload != null)
                    {
                        Attachment file = new Attachment(upload.InputStream, upload.FileName);
                        mail.Attachments.Add(file);
                    }

                    mail.Body = " Обратившееся лицо: \r\n" + legal.Sender+ "\r\n" + legal.SenderName+ "\r\n Адресс лица: \r\n" + legal.Adress + "\r\n" + legal.Email + "\r\n Обратился к: \r\n" + legal.Receiver + "\r\n Текст обращения: \r\n" + legal.Text;
                    using (SmtpClient client = new SmtpClient())
                    {
                        client.Host = "smtp.yandex.ru";
                        client.Port = 587;
                        client.EnableSsl = true;
                        client.Credentials = new NetworkCredential("appeal.rupbes@yandex.by", "Qwerty123!");
                        try
                        {
                            client.Send(mail);
                            return View("MessageSent");
                        }
                        catch (Exception)
                        {
                            ViewBag.FileMessage = "Ошибка отправки обращения";
                            return View();
                        }

                    }

                }
            }
        }
    }
}