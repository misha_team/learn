﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using rupbes.Models;

namespace rupbes.Controllers
{
    [Filters.Culture]
    public class ServiceController : Controller
    {
        Models.Database db = new Models.Database();
        private Models.DatabaseBes.Database db1 = new Models.DatabaseBes.Database();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        //УСЛУГИ
        [HttpGet]
        public ActionResult Services()
        {
            HttpCookie cookie = Request.Cookies["lang"];
            List<Services> services = db.Services.ToList();
            services.Sort((x, y) => x.title.CompareTo(y.title));
            if (cookie != null && cookie.Value == "be")
            {
                foreach (Services service in services)
                {
                    service.title = service.title_bel;
                }
            }

            return View(services);
        }
        [HttpPost]
        public ActionResult GetService(int id)
        {
            HttpCookie cookie = Request.Cookies["lang"];
            Services service = db.Services.Find(id);
            service.desc = service.desc.Replace(Environment.NewLine, "<br />");
            service.desc_bel = service.desc_bel.Replace(Environment.NewLine, "<br />");
            if (cookie != null && cookie.Value == "be")
            {
                service.title = service.title_bel;
                service.desc = service.desc_bel;
            }
            return PartialView("_GetService",service);
        }

        //Аренда
        [HttpGet]
        public ActionResult Realty()
        {
            HttpCookie cookie = Request.Cookies["lang"];
            List<Realty> realtys = db.Realty.ToList();
            realtys.Sort((x, y) => x.title.CompareTo(y.title));
            if (cookie != null && cookie.Value == "be")
            {
                foreach (Realty realty in realtys)
                {
                    realty.title = realty.title_bel;
                }
            }

            return View(realtys);
        }
        [HttpPost]
        public ActionResult GetRealty(int id)
        {
            HttpCookie cookie = Request.Cookies["lang"];
            Realty realty = db.Realty.Find(id);
            realty.desc = realty.desc.Replace(Environment.NewLine, "<br />");
            realty.desc_bel = realty.desc_bel.Replace(Environment.NewLine, "<br />");
            if (cookie != null && cookie.Value == "be")
            {
                realty.title = realty.title_bel;
                realty.desc = realty.desc_bel;
                realty.adress = realty.adress_bel;
            }
            return PartialView("_GetRealty", realty);
        }

        //Техника
        [HttpGet]
        public ActionResult Mechs()
        {
            HttpCookie cookie = Request.Cookies["lang"];
            List<Mechanisms> mechs = db.Mechanisms.ToList();
            mechs.Sort((x, y) => x.title.CompareTo(y.title));
            if (cookie != null && cookie.Value == "be")
            {
                foreach (Mechanisms mech in mechs)
                {
                    mech.title = mech.title_bel;
                }
            }

            return View(mechs);
        }
        [HttpPost]
        public ActionResult GetMech(int id)
        {
            HttpCookie cookie = Request.Cookies["lang"];
            Mechanisms mech = db.Mechanisms.Find(id);
            mech.desc = mech.desc.Replace(Environment.NewLine, "<br />");
            mech.desc_bel = mech.desc_bel.Replace(Environment.NewLine, "<br />");
            if (cookie != null && cookie.Value == "be")
            {
                mech.title = mech.title_bel;
                mech.desc = mech.desc_bel;
            }
            return PartialView("_GetMech", mech);
        }

        //Товары
        [HttpGet]
        public ActionResult Products()
        {
            var productTypes = db1.Besi_type_products.ToList();
            return View(productTypes);
        }

        [HttpPost]
        public ActionResult Products(int? id)
        {
            var besi_Price_Lists = db1.Besi_price_lists.Where(x => x.IdBesi_type_product == id).ToList();
            return PartialView("_PriceList", besi_Price_Lists);
        }
    }
}