﻿using System;
using System.Collections.Generic;
using ModelsLibrary;
using ModelsLibrary.Models;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            BesContext besContext = new BesContext();

            Organization organizations = besContext.Organizations.Find(10);

            Console.WriteLine(organizations.name);
            Console.ReadKey();
        }
    }
}
