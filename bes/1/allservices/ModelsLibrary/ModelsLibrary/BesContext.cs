﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using ModelsLibrary.Models;
using ModelsLibrary.Models.OT;
using ModelsLibrary.Models.Besi_Price;


namespace ModelsLibrary
{
    public class BesContext: DbContext
    {
        public BesContext()
        {

        }
        public BesContext(DbContextOptions dbContextOptions):base(dbContextOptions)
        {

        }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<ContactType> ContactTypes { get; set; }
        public DbSet<CurrencyType> CurrencyTypes { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<DepartmentContact> DepartmentContacts { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeContact> EmployeeContacts { get; set; }
        public DbSet<IceTradePurchase> IceTradePurchases { get; set; }
        public DbSet<IncreasedRisk> IncreasedRisks { get; set; }

        public DbSet<Organization> Organizations { get; set; }
        public DbSet<OrganizationContact> OrganizationContacts { get; set; }
        public DbSet<OrganizationRisk> OrganizationRisks { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<PersonContact> PersonContacts { get; set; }
        public DbSet<CompanyReview> CompanyReviews { get; set; }
        public DbSet<CompanyThatMadeReview> CompanyThatMadeReviews { get; set; }
        
        public DbSet<Commission> Commissions { get; set; }
        public DbSet<PostsOfCommission> PostsOfCommissions { get; set; }
        public DbSet<MemberOfCommission> MemberOfCommissions { get; set; }
        public DbSet<ExaminationProtocol> ExaminationProtocols { get; set; }
        public DbSet<ExaminationPeriod> ExaminationPeriods { get; set; }

        
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<PurchaseResult> PurchaseResults { get; set; }
        public DbSet<SpecialtyEmployee> SpecialtyEmployees { get; set; }
        public DbSet<TypeOfContract> TypeOfContracts { get; set; }
        //Zhadejko
        public DbSet<Absence> Absences { get; set; }
        public DbSet<AbsenceReason> AbsenceReasons { get; set; }
        public DbSet<AttestaionWorkplace> AttestaionWorkplaces  { get; set; }
        public DbSet<Career> Careers  { get; set; }
        public DbSet<Date> Dates  { get; set; }
        public DbSet<DeleteTalon> DeleteTalons  { get; set; }
        public DbSet<Document> Documents  { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<EducationQualification> EducationQualifications  { get; set; }
        public DbSet<EducationType> EducationTypes  { get; set; }
        public DbSet<EKTC> EKTCs  { get; set; }
        public DbSet<EKTCMedRequirement> EKTCMedRequirements  { get; set; }
        public DbSet<Examination> Examinations  { get; set; }
        public DbSet<ExaminationReason> ExaminationReasons  { get; set; }
        public DbSet<FamilyStructure> FamilyStructures { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Harm> Harms { get; set; }
        public DbSet<MedRequirement> MedRequirements { get; set; }
        public DbSet<Institution> Institutions { get; set; }
        public DbSet<RelationshipDegree> RelationshipDegrees  { get; set; }
        public DbSet<TemplateForWork> TemplateForWorks  { get; set; }
        //Zhadejko

        //Besi_price_list
        public DbSet<Besi_Price_List> Besi_Price_Lists { get; set; }
        public DbSet<Besi_type_product> Besi_Type_Products { get; set; }
        public DbSet<Besi_unit> Besi_Units { get; set; }
        public DbSet<Besi_price> Besi_Prices { get; set; }
        public DbSet<Besi_info> Besi_Infos { get; set; }
        //Besi_price_list


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {            

            if (!optionsBuilder.IsConfigured)
            {
                var connectionString = @"server=servertest\sqltest;Initial Catalog=db_test;Persist Security Info=True;User ID=sa123_boka;Password=Qwerty123!";
                optionsBuilder.UseSqlServer(connectionString, options => options.EnableRetryOnFailure())
                    .ConfigureWarnings(warnings => warnings.Throw(RelationalEventId.QueryClientEvaluationWarning));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {      
            modelBuilder.Entity<Organization>().HasMany(o => o.PurchaseCustomers).WithOne(p => p.Customer);
            modelBuilder.Entity<Organization>().HasMany(o => o.PurchaseProviders).WithOne(p => p.Provider);
            modelBuilder.Entity<Organization>().HasMany(o => o.PurchaseOrganizers).WithOne(p => p.Organizer);
            modelBuilder.Entity<Organization>().HasMany(o => o.IncreasedRisks).WithOne(p => p.Organization);
            modelBuilder.Entity<Organization>().HasMany(o => o.OrganizationContacts).WithOne(p => p.Organization);
            modelBuilder.Entity<Organization>().HasMany(o => o.Departments).WithOne(p => p.Organization);
            modelBuilder.Entity<Organization>().HasMany(o => o.CompanyReviews).WithOne(p => p.Organization);

            modelBuilder.Entity<Purchase>().HasOne(p => p.TypeOfContract).WithMany(p => p.Purchases);
            modelBuilder.Entity<Purchase>().HasOne(p => p.CurrencyType).WithMany(p => p.Purchases);
            modelBuilder.Entity<Purchase>().HasOne(p => p.PurchaseResult).WithMany(p => p.Purchases);
            modelBuilder.Entity<Purchase>().HasOne(p => p.Customer).WithMany(p => p.PurchaseCustomers);
            modelBuilder.Entity<Purchase>().HasOne(p => p.Provider).WithMany(p => p.PurchaseProviders);
            modelBuilder.Entity<Purchase>().HasOne(p => p.Organizer).WithMany(p => p.PurchaseOrganizers);

            modelBuilder.Entity<Department>().HasOne(t => t.Organization).WithMany(t => t.Departments);
            modelBuilder.Entity<Department>().HasMany(t => t.DepartmentContacts).WithOne(t => t.Department);
            modelBuilder.Entity<Department>().HasMany(t => t.Employees).WithOne(t => t.Department);

            //Zhadejko
            modelBuilder.Entity<Employee>().HasOne(t => t.Department).WithMany(t => t.Employees);
            modelBuilder.Entity<Employee>().HasOne(t => t.SpecialtyEmployee).WithMany(t => t.Employees);
            modelBuilder.Entity<Employee>().HasOne(t => t.Person).WithOne(t => t.Employee);
            modelBuilder.Entity<Employee>().HasMany(t => t.EmployeeContacts).WithOne(t => t.Employee);
            modelBuilder.Entity<Employee>().HasMany(t => t.FamilyStructures).WithOne(t => t.Employee);
            modelBuilder.Entity<Employee>().HasMany(t => t.DeleteTalons).WithOne(t => t.Employee);
            modelBuilder.Entity<Employee>().HasMany(t => t.DeleteTalonsControler).WithOne(t => t.EmployeeController);
            modelBuilder.Entity<Employee>().HasMany(t => t.AttestaionWorkplaces).WithOne(t => t.Employee);
            modelBuilder.Entity<Employee>().HasMany(t => t.Examinations).WithOne(t => t.Employee);
            modelBuilder.Entity<Employee>().HasMany(t => t.Absences).WithOne(t => t.Employee);
            modelBuilder.Entity<Employee>().HasMany(t => t.Careers).WithOne(t => t.Employee);
            modelBuilder.Entity<Employee>().HasMany(t => t.Educations).WithOne(t => t.Employee);
            modelBuilder.Entity<Employee>().HasMany(t => t.AccessToWorks).WithOne(t => t.Employee);
            modelBuilder.Entity<Employee>().HasMany(t => t.MemberOfCommissions).WithOne(t => t.Employee);
            
          
            modelBuilder.Entity<Person>().HasOne(t => t.Employee).WithOne(t => t.Person);
            modelBuilder.Entity<Person>().HasOne(t => t.Gender).WithMany(t => t.Persons);
            modelBuilder.Entity<Person>().HasMany(t => t.PersonContacts).WithOne(t => t.Person);
            modelBuilder.Entity<Person>().HasOne(t => t.Date).WithMany(t => t.Persons);
            modelBuilder.Entity<Person>().HasMany(t => t.FamilyStructures).WithOne(t => t.Person);
            //Zhadejko

            modelBuilder.Entity<OrganizationContact>().HasOne(t => t.Organization).WithMany(t => t.OrganizationContacts);
            modelBuilder.Entity<OrganizationContact>().HasOne(t => t.Contact).WithMany(t => t.OrganizationContacts);
            modelBuilder.Entity<OrganizationContact>(entity =>
            {
                entity.HasKey(e => new { e.code_p000006, e.code_p000015 });
            });

            modelBuilder.Entity<DepartmentContact>().HasOne(t => t.Department).WithMany(t => t.DepartmentContacts);
            modelBuilder.Entity<DepartmentContact>().HasOne(t => t.Contact).WithMany(t => t.DepartmentContacts);
            modelBuilder.Entity<DepartmentContact>(entity =>
            {
                entity.HasKey(e => new { e.code_p000007, e.code_p000015 });
            });

            modelBuilder.Entity<EmployeeContact>().HasOne(t => t.Contact).WithMany(t => t.EmployeeContacts);
            modelBuilder.Entity<EmployeeContact>().HasOne(t => t.Employee).WithMany(t => t.EmployeeContacts);
            modelBuilder.Entity<EmployeeContact>(entity =>
            {
                entity.HasKey(e => new { e.code_p000009, e.code_p000015 });
            });
            
            modelBuilder.Entity<PersonContact>().HasOne(t => t.Person).WithMany(t => t.PersonContacts);
            modelBuilder.Entity<PersonContact>().HasOne(t => t.Contact).WithMany(t => t.PersonContacts);
            modelBuilder.Entity<PersonContact>(entity =>
            {
                entity.HasKey(e => new { e.code_p000011, e.code_p000015 });
            });

            modelBuilder.Entity<Contact>().HasMany(t => t.OrganizationContacts).WithOne(t => t.Contact);
            modelBuilder.Entity<Contact>().HasMany(t => t.DepartmentContacts).WithOne(t => t.Contact);
            modelBuilder.Entity<Contact>().HasMany(t => t.EmployeeContacts).WithOne(t => t.Contact);
            modelBuilder.Entity<Contact>().HasMany(t => t.PersonContacts).WithOne(t => t.Contact);
            modelBuilder.Entity<Contact>().HasOne(t => t.ContactType).WithMany(t => t.Contacts);

            modelBuilder.Entity<ContactType>().HasMany(t => t.Contacts).WithOne(t => t.ContactType);

            modelBuilder.Entity<SpecialtyEmployee>().HasMany(t => t.Employees).WithOne(t => t.SpecialtyEmployee);
            
            modelBuilder.Entity<TypeOfContract>().HasMany(t => t.Purchases).WithOne(t => t.TypeOfContract);

            modelBuilder.Entity<PurchaseResult>().HasMany(t => t.Purchases).WithOne(t => t.PurchaseResult);

            modelBuilder.Entity<CurrencyType>().HasMany(t => t.Purchases).WithOne(t => t.CurrencyType);

            modelBuilder.Entity<IncreasedRisk>().HasOne(t => t.Organization).WithMany(t => t.IncreasedRisks);

            //Zhadejko
            modelBuilder.Entity<Absence>().HasOne(t => t.Employee).WithMany(t => t.Absences);
            modelBuilder.Entity<Absence>().HasOne(t => t.DateBegin).WithMany(t => t.AbsencesDateBegin);
            modelBuilder.Entity<Absence>().HasOne(t => t.DateEnd).WithMany(t => t.AbsencesDateEnd);
            modelBuilder.Entity<Absence>().HasOne(t => t.DatePeriodBegin).WithMany(t => t.AbsencesPeriodBegin);
            modelBuilder.Entity<Absence>().HasOne(t => t.DatePeriodEnd).WithMany(t => t.AbsencesPeriodEnd);
            modelBuilder.Entity<Absence>().HasOne(t => t.Document).WithOne(t => t.Absence);
            modelBuilder.Entity<Absence>().HasOne(t => t.AbsenceReason).WithMany(t => t.Absences);

            modelBuilder.Entity<AbsenceReason>().HasMany(t => t.Absences).WithOne(t => t.AbsenceReason);

            modelBuilder.Entity<AttestaionWorkplace>().HasOne(t => t.Document).WithOne(t => t.AttestaionWorkplace);
            modelBuilder.Entity<AttestaionWorkplace>().HasOne(t => t.Employee).WithMany(t => t.AttestaionWorkplaces);
            modelBuilder.Entity<AttestaionWorkplace>().HasOne(t => t.Harm).WithMany(t => t.AttestaionWorkplaces);

            modelBuilder.Entity<Career>().HasOne(t => t.TemplateForWork).WithMany(t => t.Careers);
            modelBuilder.Entity<Career>().HasOne(t => t.Document).WithOne(t => t.Career);
            modelBuilder.Entity<Career>().HasOne(t => t.Employee).WithMany(t => t.Careers);
            modelBuilder.Entity<Career>().HasOne(t => t.EKTC).WithMany(t => t.Careers);

            modelBuilder.Entity<Date>().HasMany(t => t.EducationsBegin).WithOne(t => t.DateBegin);
            modelBuilder.Entity<Date>().HasMany(t => t.EducationsEnd).WithOne(t => t.DateEnd);
            modelBuilder.Entity<Date>().HasMany(t => t.ExaminationsNextDate).WithOne(t => t.DateNextCheck);
            modelBuilder.Entity<Date>().HasMany(t => t.AbsencesDateBegin).WithOne(t => t.DateBegin);
            modelBuilder.Entity<Date>().HasMany(t => t.AbsencesDateEnd).WithOne(t => t.DateEnd);
            modelBuilder.Entity<Date>().HasMany(t => t.AbsencesPeriodBegin).WithOne(t => t.DatePeriodBegin);
            modelBuilder.Entity<Date>().HasMany(t => t.AbsencesPeriodEnd).WithOne(t => t.DatePeriodEnd);
            modelBuilder.Entity<Date>().HasMany(t => t.Persons).WithOne(t => t.Date);
            modelBuilder.Entity<Date>().HasMany(t => t.Documents).WithOne(t => t.Date);
            modelBuilder.Entity<Date>().HasMany(t => t.AccessToWorksBegin).WithOne(t => t.DateBegin);
            modelBuilder.Entity<Date>().HasMany(t => t.AccessToWorksEnd).WithOne(t => t.DateEnd);
            modelBuilder.Entity<Date>().HasMany(t => t.CareersBegin).WithOne(t => t.DateContractBegin);
            modelBuilder.Entity<Date>().HasMany(t => t.CareersEnd).WithOne(t => t.DateContractEnd);
            modelBuilder.Entity<Date>().HasMany(t => t.CompanyReviews).WithOne(t => t.DateReview);
            modelBuilder.Entity<Date>().HasMany(t => t.ExaminationProtocols).WithOne(t => t.DateProtocol);
           
            modelBuilder.Entity<DeleteTalon>().HasOne(t => t.Employee).WithMany(t => t.DeleteTalons);
            modelBuilder.Entity<DeleteTalon>().HasOne(t => t.EmployeeController).WithMany(t => t.DeleteTalonsControler);
            modelBuilder.Entity<DeleteTalon>().HasOne(t => t.Document).WithOne(t => t.DeleteTalon);

            modelBuilder.Entity<Document>().HasOne(t => t.Absence).WithOne(t => t.Document);
            modelBuilder.Entity<Document>().HasOne(t => t.Career).WithOne(t => t.Document);
            modelBuilder.Entity<Document>().HasOne(t => t.DeleteTalon).WithOne(t => t.Document);
            modelBuilder.Entity<Document>().HasOne(t => t.AttestaionWorkplace).WithOne(t => t.Document);
            modelBuilder.Entity<Document>().HasOne(t => t.Date).WithMany(t => t.Documents);
            modelBuilder.Entity<Document>().HasMany(t => t.Commissions).WithOne(t => t.DocumentCommand);
            modelBuilder.Entity<Document>().HasMany(t => t.CommandOfCommissions).WithOne(t => t.CommandOfCommission);
            modelBuilder.Entity<Document>().HasMany(t => t.DocumentProtocolArchivs).WithOne(t => t.DocumentProtocolArchiv);
            modelBuilder.Entity<Document>().HasMany(t => t.ProductsPhoto).WithOne(t => t.Photo);
            modelBuilder.Entity<Document>().HasMany(t => t.ProductsDrawing).WithOne(t => t.Drawing);

            modelBuilder.Entity<Education>().HasOne(t => t.Institution).WithMany(t => t.Educations);
            modelBuilder.Entity<Education>().HasOne(t => t.EducationType).WithMany(t => t.Educations);
            modelBuilder.Entity<Education>().HasOne(t => t.EducationQualification).WithMany(t => t.Educations);
            modelBuilder.Entity<Education>().HasOne(t => t.DateBegin).WithMany(t => t.EducationsBegin);
            modelBuilder.Entity<Education>().HasOne(t => t.DateEnd).WithMany(t => t.EducationsEnd);

            modelBuilder.Entity<EducationQualification>().HasMany(t => t.Educations).WithOne(t => t.EducationQualification);

            modelBuilder.Entity<EducationType>().HasMany(t => t.Educations).WithOne(t => t.EducationType);

            modelBuilder.Entity<EKTC>().HasMany(t => t.Careers).WithOne(t => t.EKTC);
            modelBuilder.Entity<EKTC>().HasMany(t => t.EKTCMedRequirements).WithOne(t => t.EKTC);

            modelBuilder.Entity<EKTCMedRequirement>().HasOne(t => t.EKTC).WithMany(t => t.EKTCMedRequirements);
            modelBuilder.Entity<EKTCMedRequirement>().HasOne(t => t.MedRequirement).WithMany(t => t.EKTCMedRequirements);
            modelBuilder.Entity<EKTCMedRequirement>(entity =>
            {
                entity.HasKey(e => new { e.code_p000063, e.code_p000071 });
            });

            modelBuilder.Entity<Examination>().HasOne(t => t.DateNextCheck).WithMany(t => t.ExaminationsNextDate);
            modelBuilder.Entity<Examination>().HasMany(t => t.ExaminationProtocols).WithOne(t => t.Examination);

             
            modelBuilder.Entity<FamilyStructure>().HasOne(t => t.RelationshipDegree).WithMany(t => t.FamilyStructures);
            modelBuilder.Entity<FamilyStructure>().HasOne(t => t.Person).WithMany(t => t.FamilyStructures);
            modelBuilder.Entity<FamilyStructure>().HasOne(t => t.Employee).WithMany(t => t.FamilyStructures);

            modelBuilder.Entity<Gender>().HasMany(t => t.Persons).WithOne(t => t.Gender);

            modelBuilder.Entity<Harm>().HasMany(t => t.AttestaionWorkplaces).WithOne(t => t.Harm);

            modelBuilder.Entity<MedRequirement>().HasMany(t => t.EKTCMedRequirements).WithOne(t => t.MedRequirement);

            modelBuilder.Entity<Institution>().HasMany(t => t.Educations).WithOne(t => t.Institution);

            modelBuilder.Entity<RelationshipDegree>().HasMany(t => t.FamilyStructures).WithOne(t => t.RelationshipDegree);

            modelBuilder.Entity<TemplateForWork>().HasMany(t => t.Careers).WithOne(t => t.TemplateForWork);

            // Связи таблицы организаций, оставивших отзывы
            modelBuilder.Entity<CompanyThatMadeReview>().HasMany(o => o.CompanyReviews).WithOne(p => p.CompanyThatMadeReview);

            // Связи таблицы отзывов
            modelBuilder.Entity<CompanyReview>().HasOne(o => o.CompanyThatMadeReview).WithMany(p => p.CompanyReviews);
            modelBuilder.Entity<CompanyReview>().HasOne(o => o.DateReview).WithMany(p => p.CompanyReviews);
            modelBuilder.Entity<CompanyReview>().HasOne(o => o.Organization).WithMany(p => p.CompanyReviews);
            
            // Связи таблицы комиссии проведения экзамена в ОТ
            modelBuilder.Entity<Commission>().HasOne(t => t.DocumentCommand).WithMany(t => t.Commissions);
            modelBuilder.Entity<Commission>().HasMany(t => t.MemberOfCommissions).WithOne(t => t.Commission);

            // Связи таблицы членов комиссии проверки знаний ОТ
            modelBuilder.Entity<MemberOfCommission>().HasOne(t => t.Commission).WithMany(t => t.MemberOfCommissions);
            modelBuilder.Entity<MemberOfCommission>().HasOne(t => t.PostsOfCommission).WithMany(t => t.MemberOfCommissions);
            modelBuilder.Entity<MemberOfCommission>().HasOne(t => t.Employee).WithMany(t => t.MemberOfCommissions);

            // Связи таблицы справочника должностей в комиссии проведения экзамена по ОТ
            modelBuilder.Entity<PostsOfCommission>().HasMany(t => t.MemberOfCommissions).WithOne(t => t.PostsOfCommission);

            // Связи таблицы протокола проверки знаний
            modelBuilder.Entity<ExaminationProtocol>().HasOne(t => t.DateProtocol).WithMany(t => t.ExaminationProtocols);
            modelBuilder.Entity<ExaminationProtocol>().HasOne(t => t.CommandOfCommission).WithMany(t => t.CommandOfCommissions);
            modelBuilder.Entity<ExaminationProtocol>().HasOne(t => t.Examination).WithMany(t => t.ExaminationProtocols);
            modelBuilder.Entity<ExaminationProtocol>().HasOne(t => t.ExaminationReason).WithMany(t => t.ExaminationProtocols);
            modelBuilder.Entity<ExaminationProtocol>().HasOne(t => t.DocumentProtocolArchiv).WithMany(t => t.DocumentProtocolArchivs);

            modelBuilder.Entity<ExaminationReason>().HasMany(t => t.ExaminationProtocols).WithOne(t => t.ExaminationReason);

            //Besi_price_list
            modelBuilder.Entity<Besi_unit>().HasMany(o => o.Besi_Price_Lists).WithOne(p => p.Besi_Unit);

            modelBuilder.Entity<Besi_type_product>().HasMany(o => o.Besi_Price_Lists).WithOne(p => p.Besi_Type_Product);
            modelBuilder.Entity<Besi_type_product>().HasOne(o => o.Photo).WithMany(p => p.ProductsPhoto);
            modelBuilder.Entity<Besi_type_product>().HasOne(o => o.Drawing).WithMany(p => p.ProductsDrawing);

            modelBuilder.Entity<Besi_price>().HasOne(o => o.Besi_Price_List).WithMany(p => p.Besi_Prices);
            modelBuilder.Entity<Besi_price>().HasOne(o => o.Date).WithMany(p => p.Besi_Prices);
            modelBuilder.Entity<Besi_info>().HasOne(o => o.Date).WithMany(p => p.Besi_Infos);

            modelBuilder.Entity<Besi_Price_List>().HasOne(o => o.Besi_Type_Product).WithMany(p => p.Besi_Price_Lists);
            modelBuilder.Entity<Besi_Price_List>().HasOne(o => o.Besi_Unit).WithMany(p => p.Besi_Price_Lists);
            //Besi_price_list
        }
    }
}
