﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ModelsLibrary.Models.OT;
using ModelsLibrary.Models.Besi_Price;

namespace ModelsLibrary.Models
{
    [Table("p000067")]
    public class Document
    {
        [Key]
        public int code { get; set; }
        public virtual Career Career { get; set; }
        public virtual Absence Absence { get; set; }        
        public virtual DeleteTalon DeleteTalon { get; set; }
        public virtual AttestaionWorkplace AttestaionWorkplace { get; set; }
        
        public virtual List<Commission> Commissions { get; set; }

        public virtual List<ExaminationProtocol> CommandOfCommissions { get; set; }
        public virtual List<ExaminationProtocol> DocumentProtocolArchivs { get; set; }        

        public virtual List<Besi_type_product> ProductsPhoto { get; set; }
        public virtual List<Besi_type_product> ProductsDrawing { get; set; }

        [Display(Name = "Путь к файлу")]
        public string link { get; set; }
        [Display(Name = "Номер документа")]
        public string number_document { get; set; }

        [ForeignKey(nameof(Date))]
        public int code_date { get; set; }
        public virtual Date Date { get; set; }
    }
}