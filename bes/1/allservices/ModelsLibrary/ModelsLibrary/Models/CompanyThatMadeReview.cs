﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000040")]
    public partial class CompanyThatMadeReview
    {
        [Key]
        public int code { get; set; }

        public string name { get; set; }

        public string email { get; set; }
        
        public virtual List<CompanyReview> CompanyReviews { get; set; }
    }
}