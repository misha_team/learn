﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models
{
    [Table("p000080")]
    public class Harm
    {
        [Key]
        public int code { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Категория вредности")]
        public string name { get; set; }

        public virtual List<AttestaionWorkplace> AttestaionWorkplaces { get; set; }
    }
}