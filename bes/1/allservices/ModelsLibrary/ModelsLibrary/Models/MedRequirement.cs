﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models
{
    [Table("p000071")]
    public class MedRequirement
    {
        [Key]
        public int code { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Медтребование")]
        public string name { get; set; }

        public virtual List<EKTCMedRequirement> EKTCMedRequirements { get; set; }

    }
}