﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000007")]
    public class Department
    {
        [Key]
        public int code { get; set; }
        public string name { get; set; } = "Без названия";

        [ForeignKey (nameof(Organization))]
        public int code_p000006 { get; set; }
        public virtual Organization Organization { get; set; }

        public virtual List<Employee> Employees { get; set; }
        public virtual List<DepartmentContact> DepartmentContacts { get; set; }

    }
}
