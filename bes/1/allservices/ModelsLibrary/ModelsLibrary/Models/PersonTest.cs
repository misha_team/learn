﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000083")]
    public class PersonTest
    {
        [Key]
        public int code { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Имя")]
        public string first_name { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Фамилия")]
        public string last_name { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Отчество")]
        public string father_name { get; set; }

        public int code_gender { get; set; }
        public virtual Gender Gender { get; set; }
    }
}
