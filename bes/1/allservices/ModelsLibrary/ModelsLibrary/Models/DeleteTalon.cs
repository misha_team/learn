﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models
{
    [Table("p000078")]
    public class DeleteTalon
    {
        [Key]
        public int code { get; set; }

        public int code_employee { get; set; }
        public virtual Employee Employee { get; set; }
        public int code_controller { get; set; }
        public virtual Employee EmployeeController { get; set; }

        //[Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Примечание")]
        public string notice { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Причина удаления талона")]
        public string reason { get; set; }

        [ForeignKey(nameof(Document))]
        public int code_p000067 { get; set; }
        public virtual Document Document { get; set; }
    }
}