﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ModelsLibrary.Models.OT;

namespace ModelsLibrary.Models
{
    [Table("p000009")]
    public class Employee
    {
        [Key]
        public int code { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Табельный номер")]
        public string num_tab { get; set; }
        //[Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Номер пенсионного удостоверения")]
        public string num_sertificate_pens { get; set; }
        //[Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Номер страхового удостоверения")]
        public string num_sertificate_insurance;

        [ForeignKey("Department")]
        public int code_p000007 { get; set; }
        public virtual Department Department { get; set; }

        [ForeignKey(nameof(Person))]
        public int code_p000011 { get; set; }
        public virtual Person Person { get; set; }

        [ForeignKey("SpecialtyEmployee")]
        public int code_p000008 { get; set; }
        public virtual SpecialtyEmployee SpecialtyEmployee { get; set; }

        public virtual List<AccessToWork> AccessToWorks { get; set; }
        public virtual List<Education> Educations { get; set; }
        public virtual List<FamilyStructure> FamilyStructures { get; set; }
        public virtual List<EmployeeContact> EmployeeContacts { get; set; }
        public virtual List<Absence> Absences { get; set; }
        public virtual List<Examination> Examinations { get; set; }
        public virtual List<DeleteTalon> DeleteTalons { get; set; }
        public virtual List<DeleteTalon> DeleteTalonsControler { get; set; }
        public virtual List<Career> Careers { get; set; }
        public virtual List<AttestaionWorkplace> AttestaionWorkplaces { get; set; }
        public virtual List<MemberOfCommission> MemberOfCommissions { get; set; }
        
    }
}
