﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models
{
    [Table("p000069")]
    public class RelationshipDegree
    {
        [Key]
        public int code { get; set; }
        public virtual List<FamilyStructure> FamilyStructures { get; set; }

        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Степень родства")]
        public string name { get; set; }
    }
}