﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000010")]
    public class ContactType
    {
        [Key]
        public int code { get; set; }
        public string contact_type { get; set; }

        public virtual List<Contact> Contacts { get; set; }
    }
}
