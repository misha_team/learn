﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000039")]
    public partial class CompanyReview
    {
        [Key]
        public int code { get; set; }

        [ForeignKey(nameof(Organization))]
        public int code_p000006 { get; set; }
        public virtual Organization Organization { get; set; }

        [ForeignKey(nameof(CompanyThatMadeReview))]
        public int code_p000040 { get; set; }
        public virtual CompanyThatMadeReview CompanyThatMadeReview { get; set; }

        [ForeignKey(nameof(DateReview))]
        public int code_date_review { get; set; }
        public virtual Date DateReview { get; set; }

        public bool confirm { get; set; }

        public string review_text { get; set; }

       
      
        
    }
}