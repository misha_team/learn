﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models.Besi_Price
{
    [Table("p000090")]
    public class Besi_Price_List
    {
        [Key]
        public int code { get; set; }
        public string number { get; set; }
        public string name { get; set; }
        public string volume { get; set; }
        public string weight { get; set; }

        [ForeignKey("Besi_Type_Product")]
        public int code_p000091 { get; set; }
        public virtual Besi_type_product Besi_Type_Product { get; set; }

        [ForeignKey("Besi_Unit")]
        public int code_p000092 { get; set; }
        public virtual Besi_unit Besi_Unit { get; set; }

        public virtual List<Besi_price> Besi_Prices { get; set; }
    }
}
