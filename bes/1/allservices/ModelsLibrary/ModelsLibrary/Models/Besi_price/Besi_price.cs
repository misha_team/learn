﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models.Besi_Price
{
    [Table("p000089")]
    public class Besi_price
    {
        [Key]
        public int code { get; set; }

        public string price { get; set; }
        public string price_w_nds { get; set; }

        [ForeignKey("Date")]
        public int code_p000061 { get; set; }
        public virtual Date Date { get; set; }

        [ForeignKey("Besi_Price_List")]
        public int code_p000090 { get; set; }
        public virtual Besi_Price_List Besi_Price_List { get; set; }
    }
}
