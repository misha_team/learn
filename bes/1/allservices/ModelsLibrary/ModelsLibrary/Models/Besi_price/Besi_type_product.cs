﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models.Besi_Price
{
    [Table("p000091")]
    public class Besi_type_product
    {
        [Key]
        public int code { get; set; }

        public string name { get; set; }

        public virtual List<Besi_Price_List> Besi_Price_Lists { get; set; }

        [ForeignKey("Photo")]
        public int doc_photo { get; set; }
        public virtual Document Photo { get; set; }

        [ForeignKey("Drawing")]
        public int doc_drawing { get; set; }
        public virtual Document Drawing { get; set; }
    }
}
