﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models.Besi_Price
{
    [Table("p000093")]
    public class Besi_info
    {
        [Key]
        public int code { get; set; }

        public string order { get; set; }

        [ForeignKey("Date")]
        public int code_p000061 { get; set; }
        public virtual Date Date { get; set; }

    }
}
