﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models.Besi_Price
{
    [Table("p000092")]
    public class Besi_unit
    {
        [Key]
        public int code { get; set; }

        public string name { get; set; }

        public virtual List<Besi_Price_List> Besi_Price_Lists { get; set; }
    }
}
