﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ModelsLibrary.Models.OT;

namespace ModelsLibrary.Models
{
    [Table("p000072")]
    public class ExaminationReason
    {
        [Key]
        public int code { get; set; }

        public virtual List<ExaminationProtocol> ExaminationProtocols { get; set; }

        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Причина проверки знаний")]
        public string name { get; set; }
    }
}
