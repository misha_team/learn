using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000037")]
    public class CurrencyType
    {
        [Key]
        public int Id {get; set;}
        public string Type {get; set;}
        public virtual List<Purchase> Purchases {get; set;}
    }
}