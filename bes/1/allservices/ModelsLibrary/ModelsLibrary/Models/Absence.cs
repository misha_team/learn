﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models
{
    [Table("p000076")]
    public class Absence
    {
        [Key]
        public int code { get; set; }

        [ForeignKey("Employee")]
        public int code_p000009 { get; set; }
        public virtual Employee Employee { get; set; }

        [ForeignKey("DateBegin")]
        public int code_date_begin { get; set; }
        public virtual Date DateBegin { get; set; }
        [ForeignKey("DateEnd")]
        public int code_date_end { get; set; }
        public virtual Date DateEnd { get; set; }

        [ForeignKey("DatePeriodBegin")]
        public int code_period_begin { get; set; }
        public virtual Date DatePeriodBegin { get; set; }

        [ForeignKey("DatePeriodEnd")]
        public int code_period_end { get; set; }
        public virtual Date DatePeriodEnd { get; set; }

        [ForeignKey("Document")]
        public int code_p000067 { get; set; }
        public virtual Document Document { get; set; }

        [ForeignKey("AbsenceReason")]
        public int code_p000077 { get; set; }
        public virtual AbsenceReason AbsenceReason { get; set; }
    }
}
