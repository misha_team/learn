﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models
{
    [Table("p000062")]
    public class Institution
    {
        [Key]
        public int code { get; set; }
        public virtual List<Education> Educations { get; set; }

        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Наименовнаие учебного заведения")]
        public string name { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Аббревиатура")]
        public string abreveation { get; set; }

    }
}