using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000033")]
    public class IceTradePurchase
    {
        [Key]
        public int Id {get; set;}
        public string Icetrade_id {get; set;}
        public DateTime date {get; set;}
        public string icetrade_link {get; set;}
        public string contacts {get; set;}
        public string description {get; set;}
        public DateTime uploadDate {get; set;}
    }
}