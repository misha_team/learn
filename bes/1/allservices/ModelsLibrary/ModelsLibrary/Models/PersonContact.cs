﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("s000014")]
    public class PersonContact
    {
        public int code_p000011 { get; set; }
        public virtual Person Person { get; set; }

        public int code_p000015 { get; set; }
        public virtual Contact Contact { get; set; }

    }
}
