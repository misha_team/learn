﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ModelsLibrary.Models.OT;
using ModelsLibrary.Models.Besi_Price;

namespace ModelsLibrary.Models
{
    [Table("p000061")]
    public class Date
    {
        [Key]
        public int code { get; set; }
        public virtual List<Person> Persons { get; set; }

        public virtual List<Examination> ExaminationsNextDate { get; set; }

        public virtual List<Education> EducationsBegin { get; set; }
        public virtual List<Education> EducationsEnd { get; set; }

        public virtual List<Absence> AbsencesDateBegin { get; set; }
        public virtual List<Absence> AbsencesDateEnd { get; set; }
        public virtual List<Absence> AbsencesPeriodBegin { get; set; }
        public virtual List<Absence> AbsencesPeriodEnd { get; set; }

        public virtual List<AccessToWork> AccessToWorksBegin { get; set; }
        public virtual List<AccessToWork> AccessToWorksEnd { get; set; }

        public virtual List<Career> CareersBegin { get; set; }
        public virtual List<Career> CareersEnd { get; set; }

        public virtual List<Document> Documents { get; set; }
        public virtual List<CompanyReview> CompanyReviews { get; set; }

        public virtual List<ExaminationProtocol> ExaminationProtocols { get; set; }

        public virtual List<Besi_price> Besi_Prices { get; set; }
        public virtual List<Besi_info> Besi_Infos { get; set; }

                       
        [Display(Name = "Дата")]
        public DateTime date { get; set; }
    }
}