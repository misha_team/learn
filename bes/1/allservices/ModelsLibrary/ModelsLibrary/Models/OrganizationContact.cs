﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("s000012")]
    public class OrganizationContact
    {   [ForeignKey(nameof(Organization))]    
        public int code_p000006 { get; set; }
        public virtual Organization Organization { get; set; }

        [ForeignKey(nameof(Contact))]
        public int code_p000015 { get; set; }
        public virtual Contact Contact { get; set; }
    }
}
