﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000038")]
    public class IncreasedRisk
    {
        [Key]
        public int code { get; set; }
        public string date_change { get; set; }
        public string cause { get; set; }
        public string who_included { get; set;}
        public string comment { get; set; }
        public string file_name { get; set; }

        [ForeignKey(nameof(Organization))]
        public int code_p000006 { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
