﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models
{
    [Table("p000073")]
    public class TemplateForWork
    {
        [Key]
        public int code { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Контракт")]
        public string name { get; set; }

        public virtual List<Career> Careers { get; set; }
    }
}