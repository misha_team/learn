﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ModelsLibrary.Models
{
    [Table("s000016")]
    public class EmployeeContact
    {
        public int code_p000009 { get; set; }
        public virtual Employee Employee { get; set; }

        public int code_p000015 { get; set; }
        public virtual Contact Contact { get; set; }
    }
}
