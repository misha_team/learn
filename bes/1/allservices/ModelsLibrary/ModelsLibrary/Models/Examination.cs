﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ModelsLibrary.Models.OT;

namespace ModelsLibrary.Models
{
    [Table("p000075")]
    public class Examination
    {
        [Key]
        public int code { get; set; }

        [Display(Name = "Сотрудник")]
        [ForeignKey(nameof(Employee))]
        public int code_p000009 { get; set; }
        public virtual Employee Employee { get; set; }

        [ForeignKey(nameof(DateNextCheck))]
        public int code_date_next_check { get; set; }
        public virtual Date DateNextCheck { get; set; }                      
        
        public bool reminder { get; set; }


        public virtual List<ExaminationProtocol> ExaminationProtocols { get; set; }
    }
}