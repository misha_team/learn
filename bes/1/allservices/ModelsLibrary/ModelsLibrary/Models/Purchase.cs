using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000034")]
    public class Purchase
    {
        [Key]
        public int Id {get; set;}        
        public DateTime DateReg {get; set;}
        public int NumberTT {get; set;}
        public string Content {get; set;}
        public string TTLink {get; set;}
        public DateTime DateContract {get; set;}
        public string ContractNumber {get; set;}
        public string ClosingNote {get; set;}
        public string DealAmount {get; set;} = "";

        public int TypeOfContractId {get; set;}
        public virtual TypeOfContract TypeOfContract {get; set;}
        
        public int PurchaseResultId {get; set;}
        public virtual PurchaseResult PurchaseResult {get; set;}
        
        public int CurrencyTypeId {get; set;}
        public virtual  CurrencyType CurrencyType {get; set;}

        public int CustomerId {get; set;}
        public virtual Organization Customer {get; set;}

        public int OrganizerId {get; set;}
        public virtual Organization Organizer {get; set;}
        
        public int ProviderId {get; set;}
        public virtual Organization Provider {get; set;}
    }
}