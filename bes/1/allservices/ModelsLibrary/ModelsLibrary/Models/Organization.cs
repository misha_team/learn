﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000006")]
    public class Organization
    {
        [Key]
        public int code { get; set; }
        public string name { get; set; } = "Без названия";
        public string abbreviation { get; set; } = "";
        public string unn { get; set; } = "";
        public string okpo { get; set; } = "";
        
        public virtual List<IncreasedRisk> IncreasedRisks { get; set; }
        public virtual List<OrganizationContact> OrganizationContacts { get; set; }
        public virtual List<Department> Departments { get; set; }
        public virtual List<Purchase> PurchaseCustomers { get; set; }
        public virtual List<Purchase> PurchaseOrganizers { get; set; }
        public virtual List<Purchase> PurchaseProviders { get; set; }
        public virtual List<CompanyReview> CompanyReviews { get; set; }
        
    }
}
