﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000015")]
    public class Contact
    {
        [Key]
        public int code { get; set; }        
        public string contact { get; set; }

        public int code_p000010 { get; set; }
        public virtual ContactType ContactType { get; set; }

        public virtual List<OrganizationContact> OrganizationContacts { get; set; }
        public virtual List<DepartmentContact> DepartmentContacts { get; set; }
        public virtual List<EmployeeContact> EmployeeContacts { get; set; }
        public virtual List<PersonContact> PersonContacts { get; set; }
    }
}
