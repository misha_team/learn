﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ModelsLibrary.Models
{
    [Table("s000013")]
    public class DepartmentContact
    {        
        public int code_p000007 { get; set; }
        public virtual Department Department { get; set; }

        public int code_p000015 { get; set; }
        public virtual Contact Contact { get; set; }
    }
}
