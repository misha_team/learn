﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000045")]
    public class OrganizationRisk
    {
        [Key]
        public int code { get; set; }
        public string name { get; set; }

        [Display(Name = "УНП организации для проверки")]        
        [StringLength (9, MinimumLength =9, ErrorMessage ="Ошибка: УНП организации состоит из 9 цифр!")]
        [Required(ErrorMessage = "Ошибка: УНП организации состоит из 9 цифр!")]
        [RegularExpression("[0-9]+", ErrorMessage = "Ошибка: УНП организации состоит из 9 цифр!")]
        public string unn { get; set; }
        public string date_inclusion { get; set; }
        public string cause { get; set; }
    }
}
