﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models
{
    [Table("p000074")]
    public class Career
    {
        [Key]
        public int code { get; set; }

        [ForeignKey("DateContractBegin")]
        public int code_contract_begin { get; set; }
        public virtual Date DateContractBegin { get; set; }

        [ForeignKey("DateContractEnd")]
        public int code_contract_end { get; set; }
        public virtual Date DateContractEnd { get; set; }

        [ForeignKey("Employee")]
        public int code_p000009 { get; set; }
        public virtual Employee Employee { get; set; }

        [ForeignKey("Document")]
        public int code_p000067 { get; set; }
        public virtual Document Document { get; set; }

        [ForeignKey("TemplateForWork")]
        public int code_p000073 { get; set; }
        public virtual TemplateForWork TemplateForWork { get; set; }

        [ForeignKey("EKTC")]
        public int code_p000063 { get; set; }
        public virtual EKTC EKTC { get; set; }
    }
}