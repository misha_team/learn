﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000011")]
    public class Person
    {
        [Key]
        public int code { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Имя")]
        public string first_name { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Фамилия")]
        public string last_name { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Отчество")]
        public string father_name { get; set; }

        [ForeignKey(nameof(Gender))]
        public int code_gender { get; set; }
        public virtual Gender Gender { get; set; }

        [ForeignKey(nameof(Date))]
        public int code_birthday { get; set; }
        public virtual Date Date { get; set; }

        public virtual List<PersonContact> PersonContacts { get; set; }
        public virtual List<FamilyStructure> FamilyStructures { get; set; }
        public virtual List<Education> Educations { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
