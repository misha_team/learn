﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("s000079")]
    public class EKTCMedRequirement
    {
        public int code_p000063 { get; set; }
        public virtual EKTC EKTC { get; set; }

        public int code_p000071 { get; set; }
        public virtual MedRequirement MedRequirement { get; set; }
    }
}
