﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000008")]
    public class SpecialtyEmployee
    {
        [Key]
        public int code { get; set; }
        public string name { get; set; }

        public virtual List<Employee> Employees { get; set; }

    }
}
