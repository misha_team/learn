﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models.OT
{
    [Table("p000087")]
    public class PostsOfCommission
    {
        [Key]
        public int code { get; set; }
        public int code_position { get; set; }

        public virtual List<MemberOfCommission> MemberOfCommissions { get; set; }
    }
}
