﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models.OT
{
    [Table("p000094")]
    public class ExaminationPeriod
    {
        [Key]
        public int code { get; set; }

        public int code_p000008 { get; set; }
        public int count_years { get; set; }
    }
}
