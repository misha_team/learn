﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models.OT
{
    [Table("p000085")]
    public class ExaminationProtocol
    {
        [Key]
        public int code { get; set; }

        [ForeignKey("DateProtocol")]
        public int date_protocol { get; set; }
        public virtual Date DateProtocol { get; set; }

        [ForeignKey("CommandOfCommission")]
        public int code_command_of_commission { get; set; }
        public virtual Document CommandOfCommission { get; set; }

        [ForeignKey("Examination")]
        public int code_examination { get; set; }
        public virtual Examination Examination { get; set; }

        [ForeignKey("ExaminationReason")]
        public int code_examination_reason { get; set; }
        public virtual ExaminationReason ExaminationReason { get; set; }

        [ForeignKey("DocumentProtocolArchiv")]
        public int document_archiv { get; set; }
        public virtual Document DocumentProtocolArchiv { get; set; }

        public string notice { get; set; }
        public int num_protocol { get; set; }
        public bool? result { get; set; }

    }
}
