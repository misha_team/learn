﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ModelsLibrary.Models.OT
{
    [Table("p000088")]
    public class MemberOfCommission
    {
        [Key]
        public int code { get; set; }

        [ForeignKey("Employee")]
        public int code_employee { get; set; }
        public virtual Employee Employee { get; set; }

        
        [ForeignKey("PostsOfCommission")]        
        public int post_commission { get; set; }
        public virtual PostsOfCommission PostsOfCommission { get; set; }


        [ForeignKey("Commission")]
        public int code_commission { get; set; }
        public virtual Commission Commission { get; set; }
        

    }
}
