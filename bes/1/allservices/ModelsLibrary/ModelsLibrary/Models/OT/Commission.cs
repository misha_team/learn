﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models.OT
{
    [Table("p000086")]
    public class Commission
    {
        [Key]
        public int code { get; set; }
        
        [ForeignKey("DocumentCommand")]
        public int doc { get; set; }
        public virtual Document DocumentCommand { get; set; }
        
        public virtual List<MemberOfCommission> MemberOfCommissions { get; set; }

    }
}
