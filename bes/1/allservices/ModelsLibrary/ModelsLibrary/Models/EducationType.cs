﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models
{
    [Table("p000081")]
    public class EducationType
    {
        [Key]
        public int code { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Название")]
        public string name { get; set; }

        public virtual List<Education> Educations { get; set; }
    }
}
