﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelsLibrary.Models
{
    [Table("p000066")]
    public class Gender
    {
        [Key]
        public int code { get; set; }
        [Display(Name = "Пол")]
        [Required(ErrorMessage = "Заполните поле ")]
        [RegularExpression("[А-Яа-я]{1}", ErrorMessage = "Поле не может содержать больше одного символа!")]
        public string name { get; set; }

        public virtual List<Person> Persons { get; set; }
        public virtual List<PersonTest> PersonTests { get; set; }
    }
}
