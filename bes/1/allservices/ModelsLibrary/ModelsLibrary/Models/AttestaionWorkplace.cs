﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models
{
    [Table("p000070")]
    public class AttestaionWorkplace
    {
        [Key]
        public int code { get; set; }
        //[Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Примечание")]
        public string notice { get; set; }

        public int code_p000009 { get; set; }
        public virtual Employee Employee { get; set; }

        public int code_date { get; set; }
        public virtual Date Date { get; set; }

        [ForeignKey(nameof(Document))]
        public int code_p000067 { get; set; }
        public virtual Document Document { get; set; }

        public int code_p000080 { get; set; }
        public virtual Harm Harm{ get; set; }
    }
}