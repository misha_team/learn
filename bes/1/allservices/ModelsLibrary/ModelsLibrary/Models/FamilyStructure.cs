﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models
{
    [Table("p000068")]
    public class FamilyStructure
    {
        [Key]
        public int code { get; set; }

        [ForeignKey("Employee")]
        public int code_p000009 { get; set; }
        public virtual Employee Employee { get; set; }

        [ForeignKey("RelationshipDegree")]
        public int code_p000069 { get; set; }
        public virtual RelationshipDegree RelationshipDegree { get; set; }

        [ForeignKey("Person")]
        public int code_p000011 { get; set; }
        public virtual Person Person { get; set; }
    }
}