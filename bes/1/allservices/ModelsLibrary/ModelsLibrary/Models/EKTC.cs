﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models
{
    [Table("p000063")]
    public class EKTC
    {
        [Key]
        public int code { get; set; }
        public virtual List<EKTCMedRequirement> EKTCMedRequirements { get; set; }
        public virtual List<Career> Careers { get; set; }

        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Название специальности")]
        public string name { get; set; }
    }
}