﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModelsLibrary.Models
{
    [Table("p000064")]
    public class Education
    {
        [Key]
        public int code { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Номер диплома")]
        public string diplom_number { get; set; }
        [Required(ErrorMessage = "Заполните поле ")]
        [Display(Name = "Квалификация")]

        [ForeignKey("DateBegin")]
        public int code_date_begin { get; set; }
        public virtual Date DateBegin { get; set; }
        [ForeignKey("DateEnd")]
        public int code_date_end { get; set; }
        public virtual Date DateEnd { get; set; }

        [ForeignKey("Institution")]
        public int code_p000062 { get; set; }
        public virtual Institution Institution { get; set; }

        [ForeignKey("Employee")]
        public int code_p000009 { get; set; }
        public virtual Employee Employee { get; set; }

        [ForeignKey("EducationType")]
        public int code_p000081 { get; set; }
        public virtual EducationType EducationType { get; set; }

        [ForeignKey("EducationQualification")]
        public int code_p000082 { get; set; }
        public virtual EducationQualification EducationQualification { get; set; }
    }
}