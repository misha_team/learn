﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using AllServices.Areas.Identity.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AllServices.Areas.Identity.Pages.Account.Manage
{
    public partial class IndexModel : PageModel
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;       

        public IndexModel(
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;            
        }

        [DisplayName("Имя пользователя")]
        public string Username { get; set; }

        public bool IsEmailConfirmed { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        [TempData]
        public string OldPasswordMessage { get; set; }

        [TempData]
        public string NewPasswordMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {

            [DisplayName("Имя пользователя")]
            public string Username { get; set; }

            [DisplayName("Email адрес")]
            [Required]
            [EmailAddress]
            public string Email { get; set; }
                        
            [Phone]
            [Display(Name = "Номер телефона")]
            public string PhoneNumber { get; set; }

            [PasswordPropertyText]
            [Display(Name = "Новый пароль")]
            public string NewPassword { get; set; }

            [PasswordPropertyText]
            [Display(Name = "Подтвердите новый пароль")]
            [Compare("NewPassword", ErrorMessage = "Пароли должны совпадать")]
            public string ConfirmNewPassword { get; set; }

            [PasswordPropertyText]
            [Display(Name = "Старый пароль")]
            public string OldPassword { get; set; }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Не удалось загрузить пользователя с ID '{_userManager.GetUserId(User)}'.");
            }

            var userName = await _userManager.GetUserNameAsync(user);
            var email = await _userManager.GetEmailAsync(user);
            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            IsEmailConfirmed = await _userManager.IsEmailConfirmedAsync(user);
            var isEmailConfirmed = IsEmailConfirmed;

            Username = userName;

            Input = new InputModel
            {
                Username = userName,
                Email = email,
                PhoneNumber = phoneNumber,
               
            };
            

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
                        
            var user = await _userManager.GetUserAsync(User);
            
            if (user == null)
            {
                return NotFound($"Не удалось загрузить пользователя с ID '{_userManager.GetUserId(User)}'.");
            }            

            IsEmailConfirmed = await _userManager.IsEmailConfirmedAsync(user);

            //Изменение Email
            /**
            var email = await _userManager.GetEmailAsync(user);
            if (Input.Email != email)
            {
                var setEmailResult = await _userManager.SetEmailAsync(user, Input.Email);
                if (!setEmailResult.Succeeded)
                {
                    var userId = await _userManager.GetUserIdAsync(user);
                    throw new InvalidOperationException($"Не удалось изменить адрес электронной почты для пользователя с ID '{userId}'.");
                }
            }*/



            //изменение номера телефона
            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            if (Input.PhoneNumber != phoneNumber)
            {
                var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, Input.PhoneNumber);
                if (!setPhoneResult.Succeeded)
                {
                    var userId = await _userManager.GetUserIdAsync(user);
                    throw new InvalidOperationException($"Не удалось изменить номер телефона для пользователя с ID '{userId}'.");
                }
            }

            //изменение пароля
            if (Input.OldPassword != null)
            {
                //проверка введённого текущего пароля
                bool checkPassResult = await _userManager.CheckPasswordAsync(user, Input.OldPassword);
                if (!checkPassResult)
                {
                    OldPasswordMessage = "Введён неверный текущий пароль!";
                    return Page();
                }

                if (Input.ConfirmNewPassword.Equals(Input.NewPassword))
                {
                    //проверка нового пароля на валидность
                    var passValidator = HttpContext.RequestServices.GetService(typeof(IPasswordValidator<AppUser>))
                        as IPasswordValidator<AppUser>;
                    IdentityResult passValidatorResult = await passValidator.ValidateAsync(_userManager, user, Input.NewPassword);
                    if (!passValidatorResult.Succeeded)
                    {
                        NewPasswordMessage = "Введён слишком лёгкий пароль!";
                        return Page();
                    }

                    //изменение пароля
                    IdentityResult changePassResult = await _userManager.ChangePasswordAsync(user, Input.OldPassword, Input.NewPassword);
                    if (!changePassResult.Succeeded)
                    {
                        var userId = await _userManager.GetUserIdAsync(user);
                        throw new InvalidOperationException($"Не удалось изменить пароль для прользователя с ID '{userId}'.");
                    }
                }               


            }

            //обновление пользователя
            await _signInManager.RefreshSignInAsync(user);
            StatusMessage = "Ваш профиль успешно обновлён!";
            return Page();
        }

    }
}
