﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using AllServices.Areas.Identity.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using AllServices.Extensions;
using System.Text.Encodings.Web;

namespace AllServices.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;

        public RegisterModel(
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public class InputModel
        {
            [Required(ErrorMessage = "Поле email обязательно для заполнения")]
            [EmailAddress]
            [Display(Name = "Email адрес")]
            public string Email { get; set; }

            [Required(ErrorMessage = "Поле пароль обязательно для заполнения")]
            [StringLength(100, ErrorMessage = "{0} должен быть от {2} до {1} символов длиной.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Пароль")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Подтвердите пароль")]
            [Compare("Password", ErrorMessage = "Пароли должны совпадать")]
            public string ConfirmPassword { get; set; }

            [Required(ErrorMessage = "Поле имя обязательно для заполнения")]
            [StringLength(55, ErrorMessage = "Длина имени не должна превышать 55 символов.")]
            [Display(Name ="Имя")]
            public string FirstName { get; set; }

            [Required(ErrorMessage = "Поле фамилия обязательно для заполнения")]
            [StringLength(55, ErrorMessage = "Длина фамилии не должна превышать 55 символов.")]
            [Display(Name = "Фамилия")]
            public string LastName { get; set; }

            [StringLength(55, ErrorMessage = "Длина отчества не должна превышать 55 символов.")]
            [Display(Name = "Отчество")]
            public string Patronymic { get; set; }

            [StringLength(30, ErrorMessage = "Длина имени пользователя не должна превышать 30 символов.")]
            [Display(Name = "Имя пользователя")]
            public string NickName { get; set; }
        }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (ModelState.IsValid)
            {
                var user = new AppUser { UserName = Input.Email, Email = Input.Email, FirstName = Input.FirstName, LastName = Input.LastName, NickName = Input.NickName, Patronymic =Input.Patronymic };
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    var ip = HttpContext.Connection.RemoteIpAddress.ToString();
                    _logger.LogToFileAndDatabase($"User: {@user.UserName} created a new account with password. From ip: {@ip}", user.UserName, ip);
                                                          
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,                      
                        values: new { userId = user.Id, code = code },
                        protocol: Request.Scheme);

                    await _emailSender.SendEmailAsync(Input.Email, "Подтверждение email",
                        $"Пожалуйста, подтвердите ваш адрес электронной почты, перейдя по ссылке <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>здесь</a>.");
                        
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return LocalRedirect(returnUrl);
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
                        
            return Page();
        }
    }
}
