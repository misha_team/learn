﻿using Microsoft.AspNetCore.Identity;

namespace AllServices.Areas.Identity
{
    public class CustomIdentityErrorDescriber : IdentityErrorDescriber
    {
        public override IdentityError ConcurrencyFailure() => new IdentityError() { Code = nameof(ConcurrencyFailure), Description = "Произошла ошибка при попытке совместного доступа!" };

        public override IdentityError DefaultError() => new IdentityError() { Code = nameof(DefaultError), Description = "Произошла ошибка!" };

        public override IdentityError DuplicateEmail(string email) => new IdentityError() { Code = nameof(DuplicateEmail), Description=$"Адрес: '{email}' уже существует!"};

        public override IdentityError DuplicateRoleName(string role) => new IdentityError() {Code = nameof(DuplicateRoleName), Description=$"Роль: '{role}' уже существует!"  };

        public override IdentityError DuplicateUserName(string userName) => new IdentityError() { Code = nameof(DuplicateUserName), Description =$"Пользователь: '{userName}' уже существует! "};

        public override IdentityError InvalidEmail(string email) => new IdentityError() { Code = nameof(InvalidEmail), Description = $"Некорректный email адресс:'{email}'" };

        public override IdentityError InvalidRoleName(string role) => new IdentityError() { Code = nameof(InvalidRoleName), Description = $"Некорректная роль: '{role}' не верна!" };

        public override IdentityError InvalidToken() => new IdentityError() { Code = nameof(InvalidToken), Description= "Неверный токен!" };

        public override IdentityError InvalidUserName(string userName) => new IdentityError() {Code = nameof(InvalidUserName), Description = $"Неверное имя пользователя: '{userName}'" };

        public override IdentityError LoginAlreadyAssociated() => new IdentityError() {Code = nameof(LoginAlreadyAssociated), Description ="Логин уже существует!" };

        public override IdentityError PasswordMismatch() => new IdentityError() {Code = nameof(PasswordMismatch), Description = "Пароли не совпадают!" };

        public override IdentityError PasswordRequiresDigit() => new IdentityError() { Code = nameof(PasswordRequiresDigit), Description = "Пароль должен содержать цифру!" };

        public override IdentityError PasswordRequiresLower() => new IdentityError() { Code = nameof(PasswordRequiresLower), Description = "Пароль должен содержать символ в нижнем регистре!" };

        public override IdentityError PasswordRequiresNonAlphanumeric() => new IdentityError() {Code = nameof(PasswordRequiresNonAlphanumeric), Description="Пароль должен содержать не буквенно-цифровой символ!" };

        public override IdentityError PasswordRequiresUniqueChars(int uniqueChars)
        {
            string desc = (uniqueChars>1) ? $"Пароль должен содержать {uniqueChars} уникальных символов!": "Пароль должен содержать уникальный символ!";
            return new IdentityError() {Code = nameof(PasswordRequiresUniqueChars), Description = desc};
        }
        public override IdentityError PasswordRequiresUpper() => new IdentityError() { Code = nameof(PasswordRequiresUpper), Description = "Пароль должен содержать символ в верхнем регистре!"};

        public override IdentityError PasswordTooShort(int length) => new IdentityError() { Code = nameof(PasswordTooShort), Description = $"Пароль должен быть от {length} символов!" };

        public override IdentityError RecoveryCodeRedemptionFailed() => new IdentityError() { Code = nameof(RecoveryCodeRedemptionFailed), Description ="Код восстановления не был возвращен!" };

        public override IdentityError UserAlreadyHasPassword() => new IdentityError() {Code = nameof(UserAlreadyHasPassword) , Description = "Пользователь уже имеет пароль!" };

        public override IdentityError UserAlreadyInRole(string role) => new IdentityError() {Code = nameof(UserAlreadyInRole), Description = "Пользователю уже назначена роль!" };

        public override IdentityError UserLockoutNotEnabled() => new IdentityError() { Code = nameof(UserLockoutNotEnabled), Description = "Блокировка пользователей не включена!" };

        public override IdentityError UserNotInRole(string role) => new IdentityError() { Code = nameof(UserNotInRole), Description = $"Пользователь не в роли: '{role}'" };
    }
}
