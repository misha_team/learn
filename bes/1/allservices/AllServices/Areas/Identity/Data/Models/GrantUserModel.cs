﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace AllServices.Areas.Identity.Data.Models
{
    public class GrantUserModel
    {
        public AppUser User { get; set; }
        public List<CheckedRoles> CheckedRoles {get; set;}

    }

    public class CheckedRoles
    {
        public IdentityRole Role { get; set; }
        public bool Checked { get; set; }
    }

}
