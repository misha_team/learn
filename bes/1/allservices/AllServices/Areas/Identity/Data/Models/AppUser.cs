﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace AllServices.Areas.Identity.Data.Models
{
    public class AppUser : IdentityUser
    {
        [Required]
        [MaxLength(55)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(55)]
        public string LastName { get; set; }
        [MaxLength(55)]
        public string Patronymic { get; set; }
        [MaxLength(30)]
        public string NickName { get; set; }

    }
}
