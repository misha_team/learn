﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AllServices.Areas.Identity.Data.Models;
using Castle.Core.Configuration;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AllServices.Models
{
    public class AuthorizationContext : IdentityDbContext<AppUser>
    {
        public AuthorizationContext(DbContextOptions<AuthorizationContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<AppUser> AppUsers { get; set; }
    }
}
