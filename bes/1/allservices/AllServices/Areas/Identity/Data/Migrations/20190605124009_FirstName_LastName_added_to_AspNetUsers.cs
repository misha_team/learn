﻿using AllServices.Areas.Identity.Data.Models;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AllServices.Areas.Identity.Data.Migrations
{
    public partial class FirstName_LastName_added_to_AspNetUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                table:"AspNetUsers",
                name:"FirstName",
                nullable:false,
                defaultValue:"",
                maxLength:55
                );
            migrationBuilder.AddColumn<string>(
                table:"AspNetUsers",
                name:"LastName",
                nullable:false,
                defaultValue:"",
                maxLength:55
                );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(name: "FirstName", table: "AspNetUsers");
            migrationBuilder.DropColumn(name: "LastName", table: "AspNetUsers");
        }
    }
}
