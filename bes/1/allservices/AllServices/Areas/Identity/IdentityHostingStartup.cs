﻿using System;
using System.Threading.Tasks;
using AllServices.Areas.Identity.Data.Models;
using AllServices.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(AllServices.Areas.Identity.IdentityHostingStartup))]
namespace AllServices.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<AuthorizationContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("AuthorizationContextConnection")));
                
                services.AddIdentity<AppUser, IdentityRole>()
                    .AddEntityFrameworkStores<AuthorizationContext>()
                    .AddErrorDescriber<CustomIdentityErrorDescriber>()
                    .AddTokenProvider<CustomDataProtectorTokenProvider<AppUser>>("Default");
                

                services.Configure<IdentityOptions>(options => 
                {
                    //настройки пароля
                    options.Password.RequireDigit = true;
                    options.Password.RequiredLength = 6;
                    options.Password.RequiredUniqueChars = 1;
                    options.Password.RequireUppercase = true;
                    options.Password.RequireLowercase = true;
                    //насторойки блокировки при неудачных попытках входа
                    options.Lockout.AllowedForNewUsers = true;
                    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                    options.Lockout.MaxFailedAccessAttempts = 5;
                    //настройка имен пользователей
                    options.User.AllowedUserNameCharacters =
                        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                    options.User.RequireUniqueEmail = false;


                });

                services.ConfigureApplicationCookie(options =>
                {
                    options.Cookie.HttpOnly = true;
                    options.ExpireTimeSpan = TimeSpan.FromHours(24);

                    options.LoginPath = "/Identity/Account/Login";
                    options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                    options.SlidingExpiration = true;
                    options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                });
            });
        }
    }
}