﻿using System.Collections.Generic;

namespace AllServices.Extensions
{
    public static class ListExtension
    {
        public static string ItemsToString<T>(this List<T> list)
        {
            string result="";
            foreach (var item in list)
            {
                result += $"{item.ToString()} ";
            }
            return result;
        }
    }
}
