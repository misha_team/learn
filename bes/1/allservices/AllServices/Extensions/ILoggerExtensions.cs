﻿using Microsoft.Extensions.Logging;
using Serilog;

namespace AllServices.Extensions
{
    public static class ILoggerExtensions
    {
        public static void LogToFileAndDatabase(this Microsoft.Extensions.Logging.ILogger logger, string message, params object[] args)
        {
            logger.LogInformation(message);
            Log.Information(message);
        }
    }
}
