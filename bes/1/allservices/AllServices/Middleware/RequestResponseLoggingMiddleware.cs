﻿/*
 * 06.06.2019
 * Шевченко
 * Промежуточное ПО, которое логирует все запросы пользователей и ответы сервера, ip адреса, id подключения
 */

using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http.Internal;
using AllServices.Extensions;

namespace AllServices.Middleware
{
    public class RequestResponseLoggingMiddleware
    {
        //поле, куда мы будем принимать ссылку на следующий метод в request pipeline
        private readonly RequestDelegate _next;
        private readonly Microsoft.Extensions.Logging.ILogger _logger;

        public RequestResponseLoggingMiddleware(RequestDelegate requestDelegate, ILogger<RequestResponseLoggingMiddleware> logger)
        {
            _next = requestDelegate;
            _logger = logger;
        }
        /// <summary>
        /// Метод, вызываемый CLR во время прохождения запроса по методу Configure в классе Startup
        /// </summary>
        /// контекст текущего http запроса
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            var request = await FormatRequest(context.Request);
            var originalBodyStream = context.Response.Body;
            using (var responseBody = new MemoryStream())
            {
                context.Response.Body = responseBody;
                await _next(context);
                var response = await FormatResponse(context.Response);
                var ip = context.Connection.RemoteIpAddress.ToString();
                _logger.LogToFileAndDatabase($"Request: {@request} Id: {@context.TraceIdentifier}| Response: status code: {@response} ip: {@ip}", request, response, ip, context.TraceIdentifier);

                await responseBody.CopyToAsync(originalBodyStream);
            }
        }
        /// <summary>
        /// Берет тело запроса, делает из него строку.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Строка, содержащая схему запроса, хост, путь, строку запроса и его тело</returns>
        private async Task<string> FormatRequest(HttpRequest request)
        {
            request.EnableRewind();
            request.Body.Seek(0, SeekOrigin.Begin);
            string bodyAsText = await new StreamReader(request.Body).ReadToEndAsync();
            request.Body.Seek(0, SeekOrigin.Begin);
            return $"{request.Scheme} {request.Host}{request.Path} {request.QueryString} {bodyAsText}";
        }
        /// <summary>
        /// Читает ответ, создает строку с кодом ответа сервера
        /// </summary>
        /// <param name="response"></param>
        /// <returns>Код ответа сервера</returns>
        private async Task<string> FormatResponse(HttpResponse response)
        {
            response.Body.Seek(0, SeekOrigin.Begin);
            string text = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);
            return $"{response.StatusCode}";
        }
    }
}
