﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ModelsLibrary;
using ModelsLibrary.Models;
/// <summary>
/// Author: Голубев Константин Игоревич
/// Creation date: 05.02.2019
/// </summary>
namespace AllServices.Models.CheckOrganization
{
    /// <summary>
    /// Класс предназначен для работы с таблицей p000045
    /// 
    /// Конструкторы:
    ///     - ActionsOnOrgRisk(Database Database) - принимает объект контекста БД.
    ///     
    /// Методы:
    ///     - bool CheckMatch(string value).
    /// 
    /// </summary>

    public class ActionsOnOrgRisk
    {
        public BesContext BesContext { get; set; }                          // Контекст БД
        public static List<OrganizationRisk> ListOrgRisk { get; set; }      // Список организаций с повышенным раском совершения правонарушений

        public ActionsOnOrgRisk(BesContext Database)
        {
            BesContext = Database;
        }

        internal static Task<bool> AddData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Метод осуществляет проверку на совпадения в списке юр. лиц (с риском правонарушений)
        /// </summary>
        /// <param name="value">Значение строкового типа для сравнения со списком УНП неблагонадёжных организаций</param>
        /// <returns>Тип bool: true - есть совпадение, false - совпадений нет</returns>
        public bool CheckMatch(string value)
        {
            OrganizationRisk orgRisk = BesContext.OrganizationRisks.Where(x => x.unn == value).FirstOrDefault();

            if (orgRisk == null)
            {
                return false;
            }

            return true;
        }
    }
}
