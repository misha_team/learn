﻿/// <summary>
/// Author: Саганович Александр Дмитриевич
/// Creation date: 30.04.2019
/// </summary>
namespace AllServices.Models.CheckOrganization
{
    /// <summary>
    /// Модель хранит сообщение для уведомления пользователя и цвет данного сообщения.
    /// 
    /// Конструкторы:
    ///     - MessageColor().
    ///     - MessageColor(string message, string color) - принимает строковые значения(сообщение и цвет).    
    /// </summary>
    public class MessageColor
    {
        public string message { get; set; }
        public string color { get; set; }

        public MessageColor()
        {
            color = "red";                  // По-умолчанию устанавливается красный цвет текста
        }

        public MessageColor(string message, string color)
        {
            this.message = message;
            this.color = color;
        }
    }
}
