﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO.Compression;
using System.IO;
/// <summary>
/// Author: Голубев Константин Игоревич
/// Creation date: 29.04.2019
/// </summary>
namespace AllServices.Models
{
    /// <summary>
    ///     Класс выполняет архивацию передаваемого ему в статический метод файла
    /// </summary>
    public class Archivator
    {
        /// <summary>
        /// Метод архивирует передаваемый ему файл в ZIP архив с последующим сжатием
        /// </summary>
        /// <param name="ArchivePath">Путь для сохранения архива</param>
        /// <param name="ArchiveName">Имя создаваемого архива</param>
        /// <param name="FileNames">Архивируемые файлы</param>
        /// <returns>Пустое значение</returns>
        public static void ZipArchive(string ArchivePath, string ArchiveName, List<string> FilesPath, List<string> FileNames)
        {
            using (var fileStream = new FileStream(ArchivePath + "\\" + ArchiveName, FileMode.Create))
            using (var archive = new ZipArchive(fileStream, ZipArchiveMode.Create))
            {
                for(int i =0; i < FileNames.Count; i++)
                {
                    archive.CreateEntryFromFile(FilesPath[0], FileNames[i]);
                }                
            }
        }
    }
}
