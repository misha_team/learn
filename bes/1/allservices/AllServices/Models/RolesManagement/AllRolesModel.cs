﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AllServices.Models.RolesManagement
{
    public class AllRolesModel
    {
        [Required(ErrorMessage = "Заполните поле 'Название роли'!")]
        [Display(Name = "Название роли")]
        public string Name { get; set; }
        public List<IdentityRole> Roles { get; set; }
    }
}
