﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ModelsLibrary.Models;
using System.ComponentModel.DataAnnotations;

namespace AllServices.Models.OT
{
    public class ViewModelAddExam
    {
       
        [Display(Name = "Сотрудник ")]
        public int IdPerson { get; set; }

        [Display(Name ="Протокол № ")]
        public int NumProtocol { get; set; }

        [Required(ErrorMessage = "Заполните поле!")]
        [Display(Name = "Дата проведения проверки ")]
        public DateTime DateProtocol { get; set; }

        [Display(Name ="Комиссия на основании приказа № ")]
        public Document Commission { get; set; }

        [Required(ErrorMessage = "Укажите причину проверки!")]
        [Display(Name = "Тип проверки ")]
        public ExaminationReason Reason { get; set; }

        [Required(ErrorMessage = "Выберите результат проверки!")]
        [Display(Name = "Результат проверки ")]
        public bool? Result { get; set; }

        [Required(ErrorMessage = "Заполните поле!")]
        [Display(Name = "Примечание ")]
        public string Notice { get; set; }


    }
}
