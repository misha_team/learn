﻿
namespace AllServices.Areas.Identity.Data.Models
{
    public class UserAdministrationViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string NewPassword { get; set; }
    }
}
