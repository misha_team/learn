﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace AllServices.Models.OTManagement
{
    public class ModelCommissionCommand
    {
        public int IdCommand { get; set; }

        [Display(Name = "Приказ комиссии № ")]
        public string NumAndDateCommand { get; set; }
    }
}
