﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ModelsLibrary.Models;

namespace AllServices.Models.OTManagement
{
    public class ViewSetCommissionForCheckKnowledge
    {
        [Required]
        [Display(Name = "№ приказа")]
        public int NumCommand { get; set; }

        [Required]
        [Display(Name = "Дата приказа")]
        public DateTime Date { get; set; }

        [Display(Name = "ФИО")]
        public List<int> MembersOfCommission { get; set; }
    }
}
