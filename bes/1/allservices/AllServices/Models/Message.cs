﻿
namespace AllServices.Models
{
    /// <summary>
    /// Модель для отображения сообщений
    /// </summary>
    public class Message
    {
        public string message { get; set; } //сообщение
        public string action { get; set; } //экшен
        public string controller { get; set; } //контроллер
        public string color { get; set; } //цвет

        public Message(string message, string action, string controller, string color)
        {
            this.message = message;
            this.action = action;
            this.controller = controller;
            this.color = color == "" ? "#8B0000" : color;
        }

        public Message(string message, string action, string controller)
        {
            this.message = message;
            this.action = action;
            this.controller = controller;
            this.color = "black";
        }
    }
}
