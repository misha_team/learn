﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using OfficeOpenXml;
using System.IO.Compression;
using System.Net;
using ModelsLibrary.Models;
using ModelsLibrary;
using Microsoft.Office.Interop.Excel;
using ModelsLibrary.Models.Besi_Price;
/// <summary>
/// Author: Жадейко Андрей Александровия
/// Creation date: 11.11.2019
/// </summary>
namespace AllServices.Models.LabourProtection
{
    public class ParserOT
    {
        string path = @"D:\БД\";
        BesContext context;
        public ParserOT(BesContext bescontext)
        {
            context = bescontext;
        }

        /// <summary>
        /// Метод создает создает папки с именем филиала и распаковывает туда архив,
        /// затем вызывает метод Parser
        /// </summary>
        public void ParserZip()
        {
            var pathDate = (DateTime.Today.Year).ToString() + (DateTime.Today.Month).ToString() + (DateTime.Today.Day).ToString();
            var pathD = path + "BES";
            Directory.CreateDirectory(pathD);
            var g = pathD+"_"+pathDate+".zip";
            try
            {
                ZipFile.ExtractToDirectory(g, pathD);
            }
            catch (Exception e) { }
            
            Parser(pathD+"\\");

            /*Directory.CreateDirectory(path + "BETS\\");
            ZipFile.ExtractToDirectory(path + @"\BETS_20191025.rar", path + @"\BETS"); 
            Parser(path + @"\BETS");

            Directory.CreateDirectory(path + @"\AES");
            ZipFile.ExtractToDirectory(path + @"\AES_20191025.rar", path + @"\AES"); 
            Parser(path + @"\AES");*/
        }
        /// <summary>
        /// Метод по заданному пути открывает excel файлы и забирает данные в БД
        /// </summary>
        /// <param name="path"></param>
        public void Parser(string path)
        {
            RenameExcelFile(path);
            Add_Persons(Path.GetDirectoryName(path) + @"\Персона.xlsx");
            Add_Family(Path.GetDirectoryName(path)  + @"\Состав семьи.xlsx");
            Add_Education(Path.GetDirectoryName(path)  + @"\Образование.xlsx");
            Add_Career(Path.GetDirectoryName(path)  + @"\Карьера.xlsx");            
            Add_Hospital(Path.GetDirectoryName(path)  + @"\Больничный.xlsx");            
            Add_Vacation(Path.GetDirectoryName(path)  + @"\Отпуск.xlsx");
            clearFolder(path);
        }

        /// <summary>
        /// Метод удаляет все папки по заданному пут
        /// </summary>
        /// <param name="FolderName"></param>
        private void clearFolder(string FolderName)
        {
            DirectoryInfo dir = new DirectoryInfo(FolderName);

            foreach (FileInfo fi in dir.GetFiles())
            {
                fi.Delete();
            }

            Directory.Delete(FolderName);
        }

        /// <summary>
        /// Извлечение данных про сотрудников
        /// </summary>
        /// <param name="path"></param>
        public void Add_Persons(string path) 
        {
            Employee employee;
            Person person;
            

            try
            {
                FileInfo file = new FileInfo(path);                                     // Получение информации о файле по заданному пути в переменную file
                ExcelPackage package = new ExcelPackage(file);                          // Открытие файла *.xlsx потоком вывода в переменную package
                ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();    // Чтение первого листа рабочей книги в переменную excelWorksheet
                for (int row = 3; row <= excelWorksheet.Dimension.Rows; row++)
                {
                    int gender = context.Genders  //код Пол
                    .Where(c => c.name == (string)excelWorksheet.Cells[row, 7].Value).Select(c => c.code).FirstOrDefault();
                    if (gender == 0) gender = context.Genders  
                    .Where(c => c.name == "?").Select(c => c.code).FirstOrDefault();

                    int date = context.Dates     //код Д/Р
                    .Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 9].Value).Select(c => c.code).FirstOrDefault();
                    DateTime date1 = new DateTime(1900, 1, 1);
                    if (date == 0) date = context.Dates
                    .Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();

                    person = new Person
                    {              
                            first_name = (string)excelWorksheet.Cells[row, 4].Value,
                            last_name = (string)excelWorksheet.Cells[row, 3].Value,
                            father_name = (string)excelWorksheet.Cells[row, 5].Value,
                            code_gender = gender,
                            code_birthday = date
                    } ;
                    if (!context.Persons.Any(c => c.first_name == person.first_name && c.last_name == person.last_name && c.father_name == person.father_name))                   
                    {
                        context.Persons.Add(person);
                        context.SaveChanges();
                    }
                    
                    //код Физ лица
                    int personID = context.Persons.Where(c => c.first_name == person.first_name && c.last_name == person.last_name).Select(c => c.code).FirstOrDefault();
                    string pens = (string)excelWorksheet.Cells[row, 18].Value;
                    string ins = (string)excelWorksheet.Cells[row, 17].Value;
                    if (pens == null) pens = "while young";
                    if (ins == null) ins = "still healthy";
                    employee = new Employee
                    {
                        num_tab = (string)excelWorksheet.Cells[row, 2].Value,
                        num_sertificate_pens = pens,
                        num_sertificate_insurance = ins,
                        code_p000011 = personID,
                        code_p000008 = 1, //заглушку
                        code_p000007 = 1  //добавить в запрос
                    };

                    if (!context.Employees.Any(c => c.num_tab == employee.num_tab))
                    {
                        context.Employees.Add(employee);
                        context.SaveChanges();
                    }

                }


            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
            
        }

        /// <summary>
        /// Извлечение данных про семью
        /// </summary>
        /// <param name="path"></param>
        public void Add_Family(string path) 
        {
            FamilyStructure family;
            Person person;

            try 
            {
                FileInfo file = new FileInfo(path);                                     // Получение информации о файле по заданному пути в переменную file
                ExcelPackage package = new ExcelPackage(file);                          // Открытие файла *.xlsx потоком вывода в переменную package
                ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();    // Чтение первого листа рабочей книги в переменную excelWorksheet
                for (int row = 3; row <= excelWorksheet.Dimension.Rows; row++)
                {

                    string fio = (string)excelWorksheet.Cells[row, 7].Value;
                    if (fio == null) continue;
                    string[] mas = new string[3];
                    string strOt;
                    mas = fio.Split(" ");
                    if (mas.Length < 3) strOt = "";
                    else strOt = mas[2]; //Разбивание строки на ФИО, у детей не пишут отчество

                    DateTime date1 = new DateTime(1900, 1, 1);
                    int birthday = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row,8].Value).Select(c => c.code).FirstOrDefault(); //Код Д/Р
                    if (birthday == 0) birthday = context.Dates
                    .Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();
                    int genderID = context.Genders.Where(c => c.name == "?").Select(c => c.code).FirstOrDefault(); //Заглушка на Пол

                    person = new Person
                    {
                        first_name = mas[1],
                        last_name = mas[0],
                        father_name = strOt,
                        code_birthday = birthday,
                        code_gender = genderID
                    };

                    if (!context.Persons.Any(c => c.last_name == person.last_name && c.father_name == person.father_name && c.first_name == person.first_name))
                    {
                        context.Persons.Add(person);
                        context.SaveChanges();
                    }

                    int personID = context.Persons.Where(c => c.first_name == person.first_name && c.last_name == person.last_name).Select(c => c.code).FirstOrDefault();   //код физ_лица
                    int employeeId = context.Employees.Where(c => c.num_tab == (string)excelWorksheet.Cells[row, 2].Value).Select(c => c.code).FirstOrDefault();            //код сотрудник
                    int relationshipId = context.RelationshipDegrees.Where(c => c.name == (string)excelWorksheet.Cells[row, 6].Value).Select(c => c.code).FirstOrDefault(); //код отношения
                    family = new FamilyStructure
                    {
                        code_p000011 = personID,
                        code_p000009 = employeeId,
                        code_p000069 = relationshipId
                    };

                    if (!context.FamilyStructures.Any(c => c.code_p000009 == employeeId && c.code_p000011 == personID && c.code_p000069 == relationshipId))
                    {
                        context.FamilyStructures.Add(family);
                        context.SaveChanges();
                    }

                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        /// <summary>
        /// Извлечение данных про назначение
        /// </summary>
        /// <param name="path"></param>
        public void Add_Career(string path) 
        {
            Career career;
            Document document;

            try
            {

                FileInfo file = new FileInfo(path);                                     // Получение информации о файле по заданному пути в переменную file
                ExcelPackage package = new ExcelPackage(file);                          // Открытие файла *.xlsx потоком вывода в переменную package
                ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();    // Чтение первого листа рабочей книги в переменную excelWorksheet
                for (int row = 3; row <= excelWorksheet.Dimension.Rows; row++)
                {
                    DateTime date1 = new DateTime(1900, 1, 1);
                    
                    

                    int documentDate = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 7].Value).Select(c => c.code).FirstOrDefault(); //код даты документа
                    if (documentDate == 0) documentDate = context.Dates.Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();
                    document = new Document
                    {
                        link = "Easy game - easy life",
                        code_date = documentDate,
                        number_document = (string)excelWorksheet.Cells[row, 8].Value
                    };

                    if (!context.Documents.Any(c => c.number_document == (string)excelWorksheet.Cells[row, 8].Value))
                    {
                        context.Documents.Add(document);
                        context.SaveChanges();
                    }

                    int documentID = context.Documents.Where(c => c.number_document == (string)excelWorksheet.Cells[row, 8].Value).Select(c => c.code).FirstOrDefault(); //код документа
                    int contractDate = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 6].Value).Select(c => c.code).FirstOrDefault();            //код даты контракта   
                    if (contractDate == 0) contractDate = context.Dates.Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();
                    int employeeID = context.Employees.Where(c => c.num_tab == (string)excelWorksheet.Cells[row, 2].Value).Select(c => c.code).FirstOrDefault();         //код сотрудник
                    int contractTypeID = context.TemplateForWorks.Where(c => c.name == (string)excelWorksheet.Cells[row, 5].Value).Select(c => c.code).FirstOrDefault(); //код типа контракта
                    career = new Career
                    {
                        code_p000009 = employeeID,
                        code_p000073 = contractTypeID,
                        code_p000067 = documentID,
                        code_p000063 = 1,               //Запрос написать
                        code_contract_begin = documentDate,
                        code_contract_end = documentDate
                    };

                    if (!context.Careers.Any(c => c.code_p000067 == documentID && c.code_p000009==employeeID))
                    {
                        context.Careers.Add(career);
                        context.SaveChanges();
                    }
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        /// <summary>
        /// Извлечение данных про образование
        /// </summary>
        /// <param name="path"></param>
        public void Add_Education(string path)  
        {
            Education education;
            try
            {

                FileInfo file = new FileInfo(path);                                     // Получение информации о файле по заданному пути в переменную file
                ExcelPackage package = new ExcelPackage(file);                          // Открытие файла *.xlsx потоком вывода в переменную package
                ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();    // Чтение первого листа рабочей книги в переменную excelWorksheet
                for (int row = 3; row <= excelWorksheet.Dimension.Rows; row++)
                {
                    DateTime date1 = new DateTime(1900, 1, 1);
                    int employeeID = context.Employees.Where(c => c.num_tab == (string)excelWorksheet.Cells[row, 2].Value).Select(c => c.code).FirstOrDefault();
                    if ((string)excelWorksheet.Cells[row, 3].Value=="Среднее") //Заполнение Среднего образования
                    {
                        education = new Education
                        {
                            diplom_number = " ",
                            code_p000062 = 1,       //Придумать заглушки
                            code_date_begin = 10,
                            code_date_end = 10,
                            code_p000009 = employeeID,
                            code_p000081 = 1,
                            code_p000082 = 1
                        };
                        if (!context.Educations.Any(c => c.code_p000009 == employeeID && c.diplom_number == (string)excelWorksheet.Cells[row, 8].Value))
                        {
                            context.Educations.Add(education);
                            context.SaveChanges();
                        }
                        continue;
                    }


                    int DateBegin = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 6].Value).Select(c => c.code).FirstOrDefault();  //код начало обучения
                    int DateEnd = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 7].Value).Select(c => c.code).FirstOrDefault();    //код окончания обучения
                    if (DateBegin == 0) DateBegin = context.Dates.Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();
                    if (DateEnd == 0) DateEnd = context.Dates.Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();
                    int institutionID = context.Institutions.Where(c => c.name == (string)excelWorksheet.Cells[row, 4].Value).Select(c => c.code).FirstOrDefault();     //код учебного заведения
                    if (institutionID == 0) institutionID = 1;
                    int educationTypeID = context.EducationTypes.Where(c => c.name == (string)excelWorksheet.Cells[row, 3].Value).Select(c => c.code).FirstOrDefault(); //код тип образования     
                    if (educationTypeID == 0) educationTypeID = 2;
                    int educationalQualificationID = context.EducationQualifications.Where(c => c.name == (string)excelWorksheet.Cells[row, 5].Value).Select(c => c.code).FirstOrDefault(); //код квалификации образования
                    if (educationalQualificationID == 0) educationalQualificationID = 1;
                    education = new Education
                    {
                        diplom_number = (string)excelWorksheet.Cells[row,8].Value,
                        code_p000062 = institutionID,
                        code_date_begin = DateBegin,
                        code_date_end = DateEnd,
                        code_p000009 = employeeID,
                        code_p000081 = educationTypeID,
                        code_p000082 = educationalQualificationID
                    };

                    if (!context.Educations.Any(c => c.code_p000009 == employeeID && c.diplom_number == (string)excelWorksheet.Cells[row, 8].Value))
                    {
                        context.Educations.Add(education);
                        context.SaveChanges();
                    }
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        /// <summary>
        /// Извлечение данных про больничный
        /// </summary>
        /// <param name="path"></param>
        public void Add_Hospital(string path) 
        {
            Absence absence;
            Document document;
            try
            {

                FileInfo file = new FileInfo(path);                                     // Получение информации о файле по заданному пути в переменную file
                ExcelPackage package = new ExcelPackage(file);                          // Открытие файла *.xlsx потоком вывода в переменную package
                ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();    // Чтение первого листа рабочей книги в переменную excelWorksheet
                for (int row = 3; row <= excelWorksheet.Dimension.Rows; row++)
                {
                    DateTime date1 = new DateTime(1900, 1, 1);
                    int employeeID = context.Employees.Where(c => c.num_tab == (string)excelWorksheet.Cells[row, 2].Value).Select(c => c.code).FirstOrDefault(); //код сотрудника
                    if ((string)excelWorksheet.Cells[row, 4].Value == null) continue;

                    int DateBegin = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 6].Value).Select(c => c.code).FirstOrDefault();    //код даты начало больничного  
                    int DateEnd = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 7].Value).Select(c => c.code).FirstOrDefault();      //код даты окончания больничного  
                    int PeriodBegin = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 6].Value).Select(c => c.code).FirstOrDefault(); //периоды придумать заглушки
                    int PeriodEnd = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 7].Value).Select(c => c.code).FirstOrDefault();   //
                    if (DateBegin == 0) DateBegin = context.Dates.Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();
                    if (DateEnd == 0) DateEnd = context.Dates.Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();
                    if (PeriodBegin == 0) PeriodBegin = context.Dates.Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();
                    if (PeriodEnd == 0) PeriodEnd = context.Dates.Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();


                    int documentDate = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 5].Value).Select(c => c.code).FirstOrDefault(); //код даты документа      
                    if (documentDate == 0) documentDate = context.Dates.Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();
                    document = new Document
                    {
                        link = "Easy game - easy life",
                        code_date = documentDate,
                        number_document = (string)excelWorksheet.Cells[row, 4].Value
                    };

                    if (!context.Documents.Any(c => c.number_document == (string)excelWorksheet.Cells[row, 4].Value))
                    {
                        context.Documents.Add(document);
                        context.SaveChanges();
                    }

                    int documentID = context.Documents.Where(c => c.number_document == (string)excelWorksheet.Cells[row, 4].Value).Select(c => c.code).FirstOrDefault(); //код документа   

                    absence = new Absence
                    {
                        code_date_begin = DateBegin,
                        code_date_end = DateEnd,
                        code_period_begin = PeriodBegin,
                        code_period_end = PeriodEnd,
                        code_p000009 = employeeID,
                        code_p000077 = 5,
                        code_p000067 = documentID
                    };

                    if (!context.Absences.Any(c => c.code_p000009 == employeeID && c.code_p000067 == documentID))
                    {
                        context.Absences.Add(absence);
                        context.SaveChanges();
                    }
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        /// <summary>
        /// Извлечение данных про отпуск
        /// </summary>
        /// <param name="path"></param>
        public void Add_Vacation(string path) 
        {
            Absence absence;
            Document document;
            try
            {

                FileInfo file = new FileInfo(path);                                     // Получение информации о файле по заданному пути в переменную file
                ExcelPackage package = new ExcelPackage(file);                          // Открытие файла *.xlsx потоком вывода в переменную package
                ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();    // Чтение первого листа рабочей книги в переменную excelWorksheet
                for (int row = 3; row <= excelWorksheet.Dimension.Rows; row++)
                {
                    DateTime date1 = new DateTime(1900, 1, 1);
                    int employeeID = context.Employees.Where(c => c.num_tab == (string)excelWorksheet.Cells[row, 2].Value).Select(c => c.code).FirstOrDefault(); //код сотрудника
                    if ((string)excelWorksheet.Cells[row, 3].Value == null) continue;   //проверка на отсутствие данных

                    int DateBegin = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 6].Value).Select(c => c.code).FirstOrDefault(); //код даты начала отпуска         
                    int DateEnd = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 7].Value).Select(c => c.code).FirstOrDefault();   //код даты оконсания отпуска
                    int PeriodBegin = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 4].Value).Select(c => c.code).FirstOrDefault(); //код начало периода 
                    int PeriodEnd = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 5].Value).Select(c => c.code).FirstOrDefault();   //код окончания периода
                    if (DateBegin == 0) DateBegin = context.Dates.Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();
                    if (DateEnd == 0) DateEnd = context.Dates.Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();
                    if (PeriodBegin == 0) PeriodBegin = context.Dates.Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();
                    if (PeriodEnd == 0) PeriodEnd = context.Dates.Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();


                    int documentDate = context.Dates.Where(c => c.date == (DateTime)excelWorksheet.Cells[row, 8].Value).Select(c => c.code).FirstOrDefault();   //код даты документа
                    if (documentDate == 0) documentDate = context.Dates.Where(c => c.date == date1).Select(c => c.code).FirstOrDefault();
                    document = new Document
                    {
                        link = "Easy game - easy life",
                        code_date = documentDate,
                        number_document = (string)excelWorksheet.Cells[row, 9].Value
                    };

                    if (!context.Documents.Any(c => c.number_document == (string)excelWorksheet.Cells[row, 9].Value))
                    {
                        context.Documents.Add(document);
                        context.SaveChanges();
                    }

                    int documentID = context.Documents.Where(c => c.number_document == (string)excelWorksheet.Cells[row, 9].Value).Select(c => c.code).FirstOrDefault(); //код документа

                    absence = new Absence
                    {
                        code_date_begin = DateBegin,
                        code_date_end = DateEnd,
                        code_period_begin = PeriodBegin,
                        code_period_end = PeriodEnd,
                        code_p000009 = employeeID,
                        code_p000077 = 6,
                        code_p000067 = documentID
                    };

                    if (!context.Absences.Any(c => c.code_p000009 == employeeID && c.code_p000067 == documentID))
                    {
                        context.Absences.Add(absence);
                        context.SaveChanges();
                    }
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        /// <summary>
        /// перевод excel файлов в папке(из .xls в .xlsx) и удаление исходников 
        /// </summary>
        /// <param name="path"></param>
        private void RenameExcelFile(string path) 
        {
            Console.WriteLine("Пересохраняю файл в формате .xlsx");
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            FileInfo[] fileInfoList = directoryInfo.GetFiles("*.xls");
            for (int i=0; i< fileInfoList.Length; i++)
            {
                string savePath = Path.Combine(path, fileInfoList[i].Name);

                FileInfo fileInfo = new FileInfo(savePath);
                var app = new Application();
                var converter = app.Workbooks.Open(savePath);
                savePath = savePath + "x";
                converter.SaveAs(Filename: savePath, FileFormat: XlFileFormat.xlOpenXMLWorkbook);   // Сохранение файла в формате *.xlsx
                converter.Close();
                app.Quit();
                fileInfo.Delete();
            }

        }

        public void ParserBesi(string path, DateTime dateTime)
        {
            Besi_Price_List besi_Price_List = new Besi_Price_List();
            Besi_type_product besi_Type_Product;
            Besi_unit besi_Unit;
            Besi_price besi_price;
            DateTime newdate;
            Document photo, drawing;
            int type = 0, unit = 0, price = 0, priceList = 0;
            try
            {
                FileInfo file = new FileInfo(path);                                     // Получение информации о файле по заданному пути в переменную file
                ExcelPackage package = new ExcelPackage(file);                          // Открытие файла *.xlsx потоком вывода в переменную package
                ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();    // Чтение первого листа рабочей книги в переменную excelWorksheet
                for (int row = 18; row <= excelWorksheet.Dimension.Rows; row++) //18
                {
                    if (excelWorksheet.Cells[row, 2].Value == null && excelWorksheet.Cells[row, 1].Value != null)
                    {
                        try
                        {
                            type = context.Besi_Type_Products.Where(c => c.name == (string)excelWorksheet.Cells[row, 1].Value).Select(c => c.code).FirstOrDefault();
                        }
                        catch (Exception e) { }

                        if (type == 0)
                        {
                            photo = new Document
                            {
                                code_date = context.Dates.Where(c => c.date == dateTime).FirstOrDefault().code
                            };
                            drawing = new Document
                            {
                                code_date = context.Dates.Where(c => c.date == dateTime).FirstOrDefault().code
                            };
                            context.Documents.Add(photo);
                            context.Documents.Add(drawing);
                            context.SaveChanges();

                            besi_Type_Product = new Besi_type_product
                            {
                                name = (string)excelWorksheet.Cells[row, 1].Value,
                                doc_photo = photo.code,
                                doc_drawing = drawing.code
                            };
                            context.Besi_Type_Products.Add(besi_Type_Product);
                            context.SaveChanges();
                            type = besi_Type_Product.code;
                        }
                    }
                    else if (excelWorksheet.Cells[row, 1].Value == null)
                    {
                        return;
                    }
                    else
                    {
                        try
                        {
                            unit = context.Besi_Units.Where(c => c.name == (string)excelWorksheet.Cells[row, 4].Value).Select(c => c.code).FirstOrDefault();
                        }
                        catch (Exception e) { }

                        if (unit == 0)
                        {
                            besi_Unit = new Besi_unit
                            {
                                name = (string)excelWorksheet.Cells[row, 4].Value
                            };
                            context.Besi_Units.Add(besi_Unit);
                            context.SaveChanges();
                            unit = besi_Unit.code;
                        }


                        try
                        {
                            priceList = context.Besi_Price_Lists.Where(c => c.number == (string)excelWorksheet.Cells[row, 1].Value).Select(c => c.code).FirstOrDefault();
                        }
                        catch (Exception e) { }

                        if (priceList == 0)
                        {
                            besi_Price_List = new Besi_Price_List
                            {
                                number = excelWorksheet.Cells[row, 1].Value.ToString(),
                                name = excelWorksheet.Cells[row, 3].Value.ToString(),
                                code_p000092 = unit,
                                volume = excelWorksheet.Cells[row, 5].Value.ToString(),
                                weight = excelWorksheet.Cells[row, 6].Value.ToString(),
                                code_p000091 = type
                            };
                            context.Besi_Price_Lists.Add(besi_Price_List);
                            context.SaveChanges();
                            priceList = besi_Price_List.code;
                        }


                        try
                        {
                            price = context.Besi_Prices.Where(c => c.Date.date == dateTime &&
                            c.price == excelWorksheet.Cells[row, 7].Value.ToString() &&
                            c.price_w_nds == excelWorksheet.Cells[row, 8].Value.ToString()).Select(c => c.code).FirstOrDefault();
                        }
                        catch (Exception e) { }

                        if (price == 0)
                        {
                            int Dates = context.Dates.Where(c => c.date == dateTime).Select(c => c.code).FirstOrDefault();

                            besi_price = new Besi_price
                            {
                                price = excelWorksheet.Cells[row, 7].Value.ToString(),
                                price_w_nds = excelWorksheet.Cells[row, 8].Value.ToString(),
                                code_p000061 = Dates,
                                code_p000090 = priceList
                            };

                            context.Besi_Prices.Add(besi_price);
                            context.SaveChanges();
                            price = besi_price.code;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

    }
}
