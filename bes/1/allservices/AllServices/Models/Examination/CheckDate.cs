﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AllServices.Models;
using ModelsLibrary;
using ModelsLibrary.Models;
using AllServices.Areas;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace AllServices.Models.Examination
{
    public class CheckDate
    {
        private static BesContext db;
        IEmailSender _emailSender;
        //private static List<ModelsLibrary.Models.Examination> examinations;

        public CheckDate(BesContext context, IEmailSender emailSender)
        {
            db = context;
            _emailSender = emailSender;
        }

        public List<ModelsLibrary.Models.Examination> Check(int day)
        {
            var exams = db.Examinations;
            var examinations = new List<ModelsLibrary.Models.Examination>();
            foreach (ModelsLibrary.Models.Examination e in exams)
            {
                /*
                if (e.ExaminationReason.name.ToUpper() == "ПЕРИОДИЧЕСКАЯ" || e.ExaminationReason.name.ToUpper() == "ПЕРВИЧНАЯ")
                {
                    if (e.DateNextCheck.date >= DateTime.Today && e.DateNextCheck.date <= DateTime.Today.AddDays(day))
                    {
                        examinations.Add(e);
                    }
                }
                */
            }
            return examinations;
        }

        public void SendEmail(Employee employee, DateTime date)
        {
            var list = employee.EmployeeContacts;
            foreach (EmployeeContact ec in list) //поменять на employee list
            {
                if (employee.code == ec.code_p000009)
                    if (ec.Contact.code_p000010 == 1)
                    {
                        _emailSender.SendEmailAsync(ec.Contact.contact, "Напоминание", "Easy game - easy lfie :: "+date.ToString());
                        break;
                    }
            }
          

        }

        public void SendAllEmail(List<ModelsLibrary.Models.Examination> examinations)
        {
            foreach (ModelsLibrary.Models.Examination e in examinations)
            {
                if (!e.reminder)
                {
                    //Employee employee = db.Employees.Find(e.code_p000009);
                    //Date date = db.Dates.Find(e.code_date_next_check);
                    SendEmail(e.Employee,e.DateNextCheck.date);
                    e.reminder = true ;
                }
            }
        }
    }
}
