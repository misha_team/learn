﻿using System;
using System.Collections.Generic;
using System.IO;
using AllServices.Models.OT;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using ModelsLibrary;
using ModelsLibrary.Models;
using ModelsLibrary.Models.OT;

namespace AllServices.Models.Examination
{
    public class ProtocolWord
    {
        public void CreateProtocol()//ViewModelAddExam exam, Commission commission, List<MemberOfCommission> memes, Person person, ExaminationReason reason
        {
            try
            {
                // Получаем массив байтов из нашего файла
                byte[] textByteArray = File.ReadAllBytes(@"D://тестверсия.docx");
                // Массив данных
                /*
                string comboss = memes[0].Employee.SpecialtyEmployee.name + " - " + memes[0].Employee.Person.last_name;
                string commembers = "";
                for (int i = 1; i < memes.Count; i++ )
                {
                    commembers += memes[i].Employee.SpecialtyEmployee.name + " - " + memes[i].Employee.Person.last_name + " ";
                }
                string fio = person.last_name + "^р" + person.first_name + "^р" + person.father_name;
                string depCompany = person.Employee.Department.name + " " + person.Employee.Department.Organization.abbreviation;
                string result;
                if((bool)exam.Result)
                {
                    result = "прошёл";
                }
                else
                {
                    result = "не прошёл";
                }*/
                //exam.NumProtocol.ToString(), exam.DateProtocol.ToShortDateString(), commission.DocumentCommand.number_document, commission.DocumentCommand.Date.date.ToShortDateString(),
                //comboss, commembers, person.Employee.num_tab, fio, person.Employee.SpecialtyEmployee.name, depCompany, reason.name, result, exam.Notice
                string[] data = new string[13] { "1", "2", "3", "4", "5", "111111 - 111111111^р22222222-2222222222 ^v 333333333-3333333", "7", "8", "9", "10", "11", "12", "13" };
                // Начинаем работу с потоком
                using (MemoryStream stream = new MemoryStream())
                {
                    // Записываем в поток наш word-файл
                    stream.Write(textByteArray, 0, textByteArray.Length);
                    // Открываем документ из потока с возможностью редактирования
                    using (WordprocessingDocument doc = WordprocessingDocument.Open(stream, true))
                    {
                        // Ищем все закладки в документе
                        var bookMarks = FindBookmarks(doc.MainDocumentPart.Document);

                        int i = 0;
                        foreach (var end in bookMarks)
                        {
                            // В документе встречаются какие-то служебные закладки
                            // Таким способом отфильтровываем всё ненужное
                            // end.Key содержит имена наших закладок
                            if (end.Key == "_GoBack")
                            {
                                continue;
                            }
                                
                            // Создаём текстовый элемент
                            var textElement = new Text(data[i].ToString());
                            // Далее данный текст добавляем в закладку
                            var runElement = new Run(textElement);
                            end.Value.InsertAfterSelf(runElement);
                            i++;
                        }
                    }
                    // Записываем всё в наш файл
                    File.WriteAllBytes(@"D:\Test.docx", stream.ToArray());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Во время выполнения программы произошла ошибка! Текст ошибки: {0}", ex.Message);
                Console.ReadLine();
            }
        }
    

        private static Dictionary<string, BookmarkEnd> FindBookmarks(OpenXmlElement documentPart, Dictionary<string, BookmarkEnd> outs = null, Dictionary<string, string> bStartWithNoEnds = null)
        {
            if (outs == null) { outs = new Dictionary<string, BookmarkEnd>(); }
            if (bStartWithNoEnds == null) { bStartWithNoEnds = new Dictionary<string, string>(); }

            // Проходимся по всем элементам на странице Word-документа
            foreach (var docElement in documentPart.Elements())
            {
                // BookmarkStart определяет начало закладки в рамках документа
                // маркер начала связан с маркером конца закладки
                if (docElement is BookmarkStart)
                {
                    var bookmarkStart = docElement as BookmarkStart;
                    // Записываем id и имя закладки
                    bStartWithNoEnds.Add(bookmarkStart.Id, bookmarkStart.Name);
                }

                // BookmarkEnd определяет конец закладки в рамках документа
                if (docElement is BookmarkEnd)
                {
                    var bookmarkEnd = docElement as BookmarkEnd;
                    foreach (var startName in bStartWithNoEnds)
                    {
                        // startName.Key как раз и содержит id закладки
                        // здесь проверяем, что есть связь между началом и концом закладки
                        if (bookmarkEnd.Id == startName.Key)
                            // В конечный массив добавляем то, что нам и нужно получить
                            outs.Add(startName.Value, bookmarkEnd);
                    }
                }
                // Рекурсивно вызываем данный метод, чтобы пройтись по всем элементам
                // word-документа
                FindBookmarks(docElement, outs, bStartWithNoEnds);
            }

            return outs;
        }
    }
}

