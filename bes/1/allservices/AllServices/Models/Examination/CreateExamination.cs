﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AllServices.Models.Examination
{
    public class CreateExamination
    {
        [Required(ErrorMessage = "Заполните поле!")]
        [Display(Name = "Сотрудник")]
        public int EmployeeCode { get; set; }

        [Required(ErrorMessage = "Заполните поле!")]
        [Display(Name = "Дата следующей проверки")]
        public DateTime DateNextExamination { get; set; }

        [Required(ErrorMessage = "Заполните поле!")]
        [Display(Name = "Номер документа")]
        public string NumberDoc { get; set; }

        [Required(ErrorMessage = "Заполните поле!")]
        [Display(Name = "Дата документа")]
        public DateTime DateDoc { get; set; }

        [Required(ErrorMessage = "Заполните поле!")]
        [Display(Name = "Тип проверки")]
        public int TypeExaminationCode { get; set; }

        [Display(Name = "Примечание")]
        public string Notice { get; set; }

        [Display(Name = "Результат")]
        public bool? Result { get; set; }

        [Display(Name = "Причина проверки")]
        public string Reason { get; set; }
    }
}
