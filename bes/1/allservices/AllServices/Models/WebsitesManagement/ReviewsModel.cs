﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ModelsLibrary.Models;
/// <summary>
/// Author: Голубев Константин Игоревич
/// Creation date: 29.10.2019
/// </summary>
namespace AllServices.Models.WebsitesManagement
{
    /// <summary>
    /// Класс представляет собой модель для представления _ReviewList контроллера WebsitesManagement
    /// Поля:
    ///     - DateReview - Дата создания отзыва;
    ///     - NameOrganization - Наименование организации, оставившей отзыв;
    ///     - ReviewText - Текст отзыва;
    ///     - Confirm - Отметка о разрешении публикации отзыва на сайте.
    /// </summary>
    public class ReviewsModel
    {
        public string DateReview { get; set; }
        public string NameOrganization { get; set; }        
        public string ReviewText { get; set; }
        public bool Confirm {get; set;}
    }
}