﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ModelsLibrary.Models;
/// <summary>
/// Author: Голубев Константин Игоревич
/// Creation date: 30.10.2019
/// </summary>
namespace AllServices.Models.WebsitesManagement
{
    /// <summary>
    /// Класс представляет собой модель для представлений Reviews и _ReviewList контроллера WebsitesManagement
    /// Поля:
    ///     - Organization - Организация;
    ///     - ReviewsModels - Информация о всех отзывах для данной организации.
    /// </summary>
    public class FilialModel
    {
        public Organization Organization { get; set; }
        public List<ReviewsModel> ReviewsModels { get; set; }
    }
}
