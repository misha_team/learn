﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var inputs = document.querySelectorAll('.inputfile');
Array.prototype.forEach.call(inputs, function (input) {
    var label = input.nextElementSibling,
        labelVal = label.innerHTML;

    input.addEventListener('change', function (e) {
        var fileName = '';
        if (this.files && this.files.length > 1)
            fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
        else
            fileName = e.target.value.split('\\').pop();

        if (fileName)
            label.querySelector('span').innerHTML = fileName;
        else
            label.innerHTML = labelVal;
    });
});

function confirmDelete() {
    if (confirm('Вы уверены?')) {
        return true;
    }
    else {
        return false;
    }
}

function TypeCheck() {
    var value = document.getElementById('TypeExaminationCode').value;
    //периодическая
    if (value == 1) {
        document.getElementById('DateNextBlock').style.display = 'block';
        document.getElementById('ReasonBlock').style.display = 'none';
    }//внеочередная
    else if (value == 2) {
        document.getElementById('DateNextBlock').style.display = 'none';
        document.getElementById('ReasonBlock').style.display = 'block';
    }//первичная
    else {
        document.getElementById('DateNextBlock').style.display = 'none';
        document.getElementById('ReasonBlock').style.display = 'none';
    }
}

function hover() {
    document.getElementById("add-link").innerHTML = 'Добавить';
    document.getElementById("add-link").style.fontSize = '18px';
}
function out() {
    document.getElementById("add-link").innerHTML = '+';
    document.getElementById("add-link").style.fontSize = '40px'
}
function show() {
    var element = document.getElementById("directoryes");
    var style = window.getComputedStyle(element);
    var dis = style.getPropertyValue('display');
    if (dis == 'none') {
        element.style.display = 'block';
    }
    else {
        element.style.display = 'none';
    }
}

var inputs = document.querySelectorAll('.inputfile');
Array.prototype.forEach.call(inputs, function (input) {
    var label = input.nextElementSibling,
        labelVal = label.innerHTML;

    input.addEventListener('change', function (e) {
        var fileName = '';
        if (this.files && this.files.length > 1)
            fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
        else
            fileName = e.target.value.split('\\').pop();

        if (fileName)
            label.querySelector('span').innerHTML = fileName;
        else
            label.innerHTML = labelVal;
    });
});

function TypeCheck() {
    var value = document.getElementById('TypeExaminationCode').value;
    //периодическая
    if (value == 1) {
        document.getElementById('DateNextBlock').style.display = 'block';
        document.getElementById('ReasonBlock').style.display = 'none';
    }//внеочередная
    else if (value == 2) {
        document.getElementById('DateNextBlock').style.display = 'none';
        document.getElementById('ReasonBlock').style.display = 'block';
    }//первичная
    else {
        document.getElementById('DateNextBlock').style.display = 'none';
        document.getElementById('ReasonBlock').style.display = 'none';
    }
}

function ShowProducts(code) {
    if (document.getElementById(code).getAttribute('data-before') == '▼') {
        document.getElementById('result-' + code).innerHTML = '';
        document.getElementById(code).setAttribute('data-before', '►');
    }
    else {
        document.getElementById(code).setAttribute('data-before', '▼');
        document.getElementById('id').value = code;
        document.getElementById('priceForm').setAttribute('data-ajax-update', '#result-' + code);
        document.getElementById('sub').click();
    }
}