﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AllServices.Models;
using AllServices.Models.Examination;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ModelsLibrary;
using ModelsLibrary.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using AllServices.Models.OT;
using ModelsLibrary.Models.OT;

namespace AllServices.Controllers
{
    public class OTController : Controller
    {
        private static BesContext _db;
        private IHostingEnvironment _env;
        private readonly IEmailSender _emailSender;
        private static List<Examination> checkExaminations;
        private static CheckDate checkDate;

        public OTController(BesContext besContext, IHostingEnvironment env, IEmailSender emailSender)
        {
            _db = besContext;
            _env = env;
            _emailSender = emailSender;
        }

        [Authorize(Roles = "user")]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "manager")]
        //Проверка знаний
        [HttpGet]
        public IActionResult AllExaminations(string message)
        {
            var exams = _db.Examinations.ToList();
            ViewBag.Message = message;
            return View(exams);
        }

        [Authorize(Roles = "manager")]
        [HttpGet]
        public IActionResult CheckExaminations()
        {
            checkDate = new CheckDate(_db, _emailSender);
            checkExaminations = checkDate.Check(7);
            return View(checkExaminations);
        }

        [Authorize(Roles = "manager")]
        public IActionResult Reminder()
        {
            checkDate.SendAllEmail(checkExaminations);
            return View(checkExaminations);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [Authorize(Roles = "manager")]
        [HttpGet]
        public IActionResult NewExamination(Employee employee = null)
        {
            ViewModelAddExam viewModel = new ViewModelAddExam();

            var examinationProtocols = _db.ExaminationProtocols.ToList();
            int numProtocol = 0;

            if (examinationProtocols.Count != 0)
            {
                ExaminationProtocol examinationProtocol = examinationProtocols.OrderByDescending(x => x.num_protocol).First();
                examinationProtocol.num_protocol++;
                numProtocol = examinationProtocol.num_protocol;
            }
            else
            {
                numProtocol = 1;
            }

            viewModel.NumProtocol = numProtocol;


            if (employee.code != 0)
            {
                Person person = _db.Persons.Find(employee.Person.code);
                viewModel.IdPerson = person.code;

            }


            List<ExaminationReason> ExaminationReasons = _db.ExaminationReasons.ToList();
            IEnumerable<ExaminationReason> listExaminationReasons = (IEnumerable<ExaminationReason>)ExaminationReasons;
            ViewBag.selectExaminationReasons = new SelectList(listExaminationReasons, "code", "name");

            var commissions = _db.Commissions.ToList();
            List<Document> commands = new List<Document>();
            if (commissions.Count != 0)
            {
                
                foreach(Commission commission in commissions)
                {
                    Document docCommand = _db.Documents.Find(commission.doc);
                    commands.Add(docCommand);
                }
            }

            IEnumerable<Document> listCommissions = (IEnumerable<Document>)commands;
            ViewBag.selectCommissions = new SelectList(listCommissions, "code", "number_document");
           

            return View(viewModel);

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [Authorize(Roles = "manager")]
        [HttpPost]
        public IActionResult NewExamination(ViewModelAddExam viewModel, [FromForm] IFormFile file)
        {
            
            Document document = new Document();


            if (file != null)
            {
                List<string> filesPath = new List<string>();

                long dateticks1 = DateTime.Now.Ticks;
                long datemilliseconds1 = dateticks1 / 10000;


                string fileName = String.Format(@"Content\Files\OT\{0}_{1}", datemilliseconds1, file.FileName);
                String savePath = Path.Combine(_env.WebRootPath, fileName);

                filesPath.Add(savePath);


                using (FileStream fileStream = new FileStream(savePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }

                //lololo


                /*
                Commission com = _db.Commissions.Where(x => x.doc == viewModel.Commission.code).First();

                List<MemberOfCommission> memes = _db.MemberOfCommissions.Where(x => x.code_commission == com.code).ToList();

                Employee employee = _db.Employees.Where(x => x.code_p000011 == viewModel.IdPerson).First();

                Person person = _db.Persons.Where(x => x.code == viewModel.IdPerson).First();

                ExaminationReason reason = _db.ExaminationReasons.Where(x => x.code == viewModel.Reason.code).First();
                */
                ProtocolWord pr = new ProtocolWord();
                pr.CreateProtocol();//viewModel, com, memes, person, reason

                //lololo
                /*
                 * Санёк зденсь генерирует файл ворд
                 * 
                 * 
                 * 
                 * 
                 * filesPath.Add(savePath2);
                 * */


                // Добавление архива с документами и сгенерированного файла протокола в архив


                string archivPath = String.Format(@"Content\Files\OT\Archiv");
                String rootArchivPath = Path.Combine(_env.WebRootPath, archivPath);

                string archivName = String.Format(@"{0}.{1}.{2}_{3}.zip", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, datemilliseconds1);
                
                List<string> fileNames = new List<string>();
                fileNames.Add(String.Format(@"{0}_{1}", datemilliseconds1, file.FileName));


                long dateticks2 = DateTime.Now.Ticks;
                long datemilliseconds2 = dateticks2 / 10000;
                string fileName2 = String.Format(@"Content\Files\OT\{0}_{1}", datemilliseconds2, file.FileName);
                String savePath2 = Path.Combine(_env.WebRootPath, fileName);

                filesPath.Add(savePath2);


                fileNames.Add(String.Format(@"{0}_{1}", datemilliseconds2, file.FileName));

                Archivator.ZipArchive(rootArchivPath, archivName, filesPath, fileNames);

                String savePathArchiv = Path.Combine(rootArchivPath, archivName);


                document.link = savePathArchiv;
            }



            ExaminationProtocol protocol = new ExaminationProtocol();

            protocol.num_protocol = viewModel.NumProtocol;
            

            Examination examination = new Examination();
            examination.Employee =_db.Employees.Where(x => x.Person.code == viewModel.IdPerson).FirstOrDefault();

            int countYears = _db.ExaminationPeriods.Where(x => x.code_p000008 == examination.Employee.code_p000008).FirstOrDefault().count_years;

            protocol.DateProtocol = _db.Dates.Where(x => x.date == viewModel.DateProtocol).FirstOrDefault();

            DateTime dateNaxt = new DateTime(protocol.DateProtocol.date.Year + countYears, protocol.DateProtocol.date.Month, protocol.DateProtocol.date.Day);

            examination.DateNextCheck = _db.Dates.Where(x => x.date == dateNaxt).FirstOrDefault();


            document.Date = protocol.DateProtocol;
            protocol.DocumentProtocolArchiv = document;

            protocol.Examination = examination;

            protocol.CommandOfCommission = _db.Documents.Where(x => x.code == viewModel.Commission.code).FirstOrDefault();

            protocol.ExaminationReason = _db.ExaminationReasons.Where(x => x.code == viewModel.Reason.code).FirstOrDefault();

            protocol.result = viewModel.Result;
            
            protocol.notice = viewModel.Notice;

            

            _db.ExaminationProtocols.Add(protocol);
            _db.SaveChanges();

            

            
            return PartialView("_NewExaminationMessage", dateNaxt.ToShortDateString());
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult NewExaminationPartial(string searchText)
        {
            searchText = searchText.ToUpper();

            List<Person> foundPeople = new List<Person>();

            List<Person> people = _db.Persons.ToList();

            string buffer;

            foreach(Person person in people)
            {
                buffer = (person.last_name + person.first_name + person.father_name).ToUpper();

                if(buffer.IndexOf(searchText) > -1 && person.Employee != null)
                {
                    foundPeople.Add(person);
                }
            }


            return PartialView("_NewExaminationPartial", foundPeople);
        }


    }
}