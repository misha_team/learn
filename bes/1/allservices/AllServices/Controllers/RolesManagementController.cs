﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AllServices.Models.RolesManagement;
using Microsoft.AspNetCore.Authorization;

namespace AllServices.Controllers
{
    [Authorize(Roles = "admin")]
    public class RolesManagementController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<UserManagementController> _logger;

        public RolesManagementController(RoleManager<IdentityRole> roleManager, ILogger<UserManagementController> logger)
        {
            _roleManager = roleManager;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index()
        {
            AllRolesModel model = new AllRolesModel() { Roles = _roleManager.Roles.ToList() };
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> DeleteRole(string id)
        {
            if (id != null)
            {
                var db_role = await _roleManager.FindByIdAsync(id);
                if (db_role != null)
                {
                    var result = await _roleManager.DeleteAsync(db_role);
                    if (result.Succeeded)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                }
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(AllRolesModel allRoles)
        {
            if (ModelState.IsValid)
            {
                var exist = await _roleManager.RoleExistsAsync(allRoles.Name);
                if (!exist)
                {
                    var result = await _roleManager.CreateAsync(new IdentityRole(allRoles.Name));
                    if (result.Succeeded)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ModelState.AddModelError("", "Произошла ошибка при добавлении");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Роль уже существует");
                }
            }
            else
            {
                ModelState.AddModelError("", "Неверное имя роли");
            }
            allRoles.Roles = _roleManager.Roles.ToList();
            return View("Index", allRoles);
        }

    }
}