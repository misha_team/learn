﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AllServices.Models.CheckOrganization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AllServices.Controllers
{
    
    public class HomeController : Controller
    {
        //private readonly IEmailSender _emailSender;
        //private readonly ILogger<HomeController> _logger;
        public HomeController(IEmailSender emailSender, ILogger<HomeController> logger)
        {

        }

        [Authorize]
        public async Task<IActionResult> Index()
        {            
            return View();
        }

    }
}