﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ModelsLibrary;
using ModelsLibrary.Models;
using AllServices.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using AllServices.Models.LabourProtection;
using Microsoft.AspNetCore.Authorization;

namespace AllServices.Controllers
{
    public class DirectoryController : Controller
    {
        private readonly BesContext _db;
        private IHostingEnvironment _env;

        public DirectoryController(BesContext besContext, IHostingEnvironment env)
        {
            _db = besContext;
            _env = env;
        }

        [Authorize(Roles = "user")]
        public IActionResult Index()
        {
            return View();
        }

        //справочник гендеров
        [Authorize(Roles = "user")]
        [HttpGet]
        public IActionResult AllGenders(string message)
        {
            var genders = _db.Genders.ToList();
            ViewBag.Message = message;
            return View(genders);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult AddGender()
        {
            return View();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult AddGender(Gender gender)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.Genders.Add(gender);
                    _db.SaveChanges();
                    message = "Запись добавлена!";
                    color = "#006400";
                }
                catch (DbUpdateException)
                {
                    message = "Данный пол уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message wm = new Message(message, "AllGenders", "Directory", color);
            return PartialView("../_ViewMessage", wm);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult EditGender(int? id)
        {
            if (id != null)
            {
                Gender gender = _db.Genders.Find(id);
                if (gender != null)
                {
                    return View(gender);
                }
            }
            return NotFound();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult EditGender(Gender gender)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.Genders.Update(gender);
                    _db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    message = "Данный пол уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message m = new Message(message, "AllGenders", "Directory", color);
            return PartialView("../_ViewMessage", m);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult DeleteGender(int? id)
        {
            string message = "";
            if (id != null)
            {
                Gender gender = _db.Genders.Find(id);
                if (gender != null)
                {
                    try
                    {
                        _db.Genders.Remove(gender);
                        _db.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        message = "Невозможно удалить! Данный пол используется.";
                    }
                }
            }
            return RedirectToAction("AllGenders", new { message });
        }

        //справочник причин опоздания
        [Authorize(Roles = "user")]
        [HttpGet]
        public IActionResult AllAbsenceReasons(string message)
        {
            var reasons = _db.AbsenceReasons.ToList();
            ViewBag.Message = message;
            return View(reasons);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult AddAbsenceReason()
        {
            return View();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult AddAbsenceReason(AbsenceReason reason)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.AbsenceReasons.Add(reason);
                    _db.SaveChanges();
                    message = "Запись добавлена!";
                    color = "#006400";
                }
                catch (DbUpdateException)
                {
                    message = "Данная причина уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message wm = new Message(message, "AllAbsenceReasons", "Directory", color);
            return PartialView("../_ViewMessage", wm);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult EditAbsenceReason(int? id)
        {
            if (id != null)
            {
                AbsenceReason reason = _db.AbsenceReasons.Find(id);
                if (reason != null)
                {
                    return View(reason);
                }
            }
            return NotFound();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult EditAbsenceReason(AbsenceReason reason)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.AbsenceReasons.Update(reason);
                    _db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    message = "Данная причина уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message m = new Message(message, "AllAbsenceReasons", "Directory", color);
            return PartialView("../_ViewMessage", m);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult DeleteAbsenceReason(int? id)
        {
            string message = "";
            if (id != null)
            {
                AbsenceReason reason = _db.AbsenceReasons.Find(id);
                if (reason != null)
                {
                    try
                    {
                        _db.AbsenceReasons.Remove(reason);
                        _db.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        message = "Невозможно удалить! Данная причина используется.";
                    }
                }
            }
            return RedirectToAction("AllAbsenceReasons", new { message });
        }

        //Справочник Институтов
        [Authorize(Roles = "user")]
        [HttpGet]
        public IActionResult AllInstitutions(string message)
        {
            var institutions = _db.Institutions.ToList();
            ViewBag.Message = message;
            return View(institutions);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult AddInstitution()
        {
            return View();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult AddInstitution(Institution institution)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.Institutions.Add(institution);
                    _db.SaveChanges();
                    message = "Запись добавлена!";
                    color = "#006400";
                }
                catch (DbUpdateException)
                {
                    message = "Данное учебное заведение уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message wm = new Message(message, "AllInstitutions", "Directory", color);
            return PartialView("../_ViewMessage", wm);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult EditInstitution(int? id)
        {
            if (id != null)
            {
                Institution institution = _db.Institutions.Find(id);
                if (institution != null)
                {
                    return View(institution);
                }
            }
            return NotFound();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult EditInstitution(Institution institution)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.Institutions.Update(institution);
                    _db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    message = "Данное учебное заведение уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message m = new Message(message, "AllAbsenceReasons", "Directory", color);
            return PartialView("../_ViewMessage", m);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult DeleteInstitution(int? id)
        {
            string message = "";
            if (id != null)
            {
                Institution institution = _db.Institutions.Find(id);
                if (institution != null)
                {
                    try
                    {
                        _db.Institutions.Remove(institution);
                        _db.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        message = "Невозможно удалить! Данный пол используется.";
                    }
                }
            }
            return RedirectToAction("AllInstitutions", new { message });
        }

        //Справочник Институтов
        [Authorize(Roles = "user")]
        [HttpGet]
        public IActionResult AllMedRequirements(string message)
        {
            var requirements = _db.MedRequirements.ToList();
            ViewBag.Message = message;
            return View(requirements);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult AddMedRequirement()
        {
            return View();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult AddMedRequirement(MedRequirement requirement)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.MedRequirements.Add(requirement);
                    _db.SaveChanges();
                    message = "Запись добавлена!";
                    color = "#006400";
                }
                catch (DbUpdateException)
                {
                    message = "Данное учебное заведение уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message wm = new Message(message, "AllMedRequirements", "Directory", color);
            return PartialView("../_ViewMessage", wm);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult EditMedRequirement(int? id)
        {
            if (id != null)
            {
                MedRequirement requirement = _db.MedRequirements.Find(id);
                if (requirement != null)
                {
                    return View(requirement);
                }
            }
            return NotFound();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult EditMedRequirement(MedRequirement requirement)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.MedRequirements.Update(requirement);
                    _db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    message = "Данное учебное заведение уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message m = new Message(message, "AllMedRequirements", "Directory", color);
            return PartialView("../_ViewMessage", m);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult DeleteMedRequirement(int? id)
        {
            string message = "";
            if (id != null)
            {
                MedRequirement requirement = _db.MedRequirements.Find(id);
                if (requirement != null)
                {
                    try
                    {
                        _db.MedRequirements.Remove(requirement);
                        _db.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        message = "Невозможно удалить! Данный пол используется.";
                    }
                }
            }
            return RedirectToAction("AllMedRequirements", new { message });
        }

        //справочник типов образования
        [Authorize(Roles = "user")]
        [HttpGet]
        public IActionResult AllEducationTypes(string message)
        {
            var types = _db.EducationTypes.ToList();
            ViewBag.Message = message;
            return View(types);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult AddEducationType()
        {
            return View();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult AddEducationType(EducationType educationType)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.EducationTypes.Add(educationType);
                    _db.SaveChanges();
                    message = "Запись добавлена!";
                    color = "#006400";
                }
                catch (DbUpdateException)
                {
                    message = "Данный пол уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message wm = new Message(message, "AllEducationTypes", "Directory", color);
            return PartialView("../_ViewMessage", wm);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult EditEducationType(int? id)
        {
            if (id != null)
            {
                EducationType educationType = _db.EducationTypes.Find(id);
                if (educationType != null)
                {
                    return View(educationType);
                }
            }
            return NotFound();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult EditEducationType(EducationType educationType)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.EducationTypes.Update(educationType);
                    _db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    message = "Данный пол уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message m = new Message(message, "AllEducationTypes", "Directory", color);
            return PartialView("../_ViewMessage", m);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult DeleteEducationType(int? id)
        {
            string message = "";
            if (id != null)
            {
                EducationType educationType = _db.EducationTypes.Find(id);
                if (educationType != null)
                {
                    try
                    {
                        _db.EducationTypes.Remove(educationType);
                        _db.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        message = "Невозможно удалить! Данный пол используется.";
                    }
                }
            }
            return RedirectToAction("AllEducationTypes", new { message });
        }

        //справочник квалификаций образований
        [Authorize(Roles = "user")]
        [HttpGet]
        public IActionResult AllEducationQualifications(string message)
        {
            var qualifications = _db.EducationQualifications.ToList();
            ViewBag.Message = message;
            return View(qualifications);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult AddEducationQualification()
        {
            return View();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult AddEducationQualification(EducationQualification educationQualification)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.EducationQualifications.Add(educationQualification);
                    _db.SaveChanges();
                    message = "Запись добавлена!";
                    color = "#006400";
                }
                catch (DbUpdateException)
                {
                    message = "Данный пол уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message wm = new Message(message, "AllEducationQualifications", "Directory", color);
            return PartialView("../_ViewMessage", wm);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult EditEducationQualification(int? id)
        {
            if (id != null)
            {
                EducationQualification educationQualification = _db.EducationQualifications.Find(id);
                if (educationQualification != null)
                {
                    return View(educationQualification);
                }
            }
            return NotFound();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult EditEducationQualification(EducationQualification educationQualification)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.EducationQualifications.Update(educationQualification);
                    _db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    message = "Данный пол уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message m = new Message(message, "AllEducationQualifications", "Directory", color);
            return PartialView("../_ViewMessage", m);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult DeleteEducationQualification(int? id)
        {
            string message = "";
            if (id != null)
            {
                EducationQualification educationQualification = _db.EducationQualifications.Find(id);
                if (educationQualification != null)
                {
                    try
                    {
                        _db.EducationQualifications.Remove(educationQualification);
                        _db.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        message = "Невозможно удалить! Данный пол используется.";
                    }
                }
            }
            return RedirectToAction("AllEducationQualifications", new { message });
        }

        //справочник гендеров
        [Authorize(Roles = "user")]
        [HttpGet]
        public IActionResult AllExaminationReasons(string message)
        {
            var reasons= _db.ExaminationReasons.ToList();
            ViewBag.Message = message;
            return View(reasons);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult AddExaminationReason()
        {
            return View();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult AddExaminationReason(ExaminationReason examinationReason)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.ExaminationReasons.Add(examinationReason);
                    _db.SaveChanges();
                    message = "Запись добавлена!";
                    color = "#006400";
                }
                catch (DbUpdateException)
                {
                    message = "Данный пол уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message wm = new Message(message, "AllExaminationReasons", "Directory", color);
            return PartialView("../_ViewMessage", wm);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult EditExaminationReason(int? id)
        {
            if (id != null)
            {
                ExaminationReason examinationReason = _db.ExaminationReasons.Find(id);
                if (examinationReason != null)
                {
                    return View(examinationReason);
                }
            }
            return NotFound();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult EditExaminationReason(ExaminationReason examinationReason)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.ExaminationReasons.Update(examinationReason);
                    _db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    message = "Данный пол уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message m = new Message(message, "AllExaminationReasons", "Directory", color);
            return PartialView("../_ViewMessage", m);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult DeleteExaminationReason(int? id)
        {
            string message = "";
            if (id != null)
            {
                ExaminationReason examinationReason = _db.ExaminationReasons.Find(id);
                if (examinationReason != null)
                {
                    try
                    {
                        _db.ExaminationReasons.Remove(examinationReason);
                        _db.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        message = "Невозможно удалить! Данный пол используется.";
                    }
                }
            }
            return RedirectToAction("AllExaminationReasons", new { message });
        }
        
        //справочник степени родства
        [Authorize(Roles = "user")]
        [HttpGet]
        public IActionResult AllRelationshipDegrees(string message)
        {
            var degrees = _db.RelationshipDegrees.ToList();
            ViewBag.Message = message;
            return View(degrees);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult AddRelationshipDegree()
        {
            return View();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult AddRelationshipDegree(RelationshipDegree relationshipDegree)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.RelationshipDegrees.Add(relationshipDegree);
                    _db.SaveChanges();
                    message = "Запись добавлена!";
                    color = "#006400";
                }
                catch (DbUpdateException)
                {
                    message = "Данный пол уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message wm = new Message(message, "AllRelationshipDegrees", "Directory", color);
            return PartialView("../_ViewMessage", wm);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult EditRelationshipDegree(int? id)
        {
            if (id != null)
            {
                RelationshipDegree relationshipDegree = _db.RelationshipDegrees.Find(id);
                if (relationshipDegree != null)
                {
                    return View(relationshipDegree);
                }
            }
            return NotFound();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult EditRelationshipDegree(RelationshipDegree relationshipDegree)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.RelationshipDegrees.Update(relationshipDegree);
                    _db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    message = "Данный пол уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message m = new Message(message, "AllRelationshipDegrees", "Directory", color);
            return PartialView("../_ViewMessage", m);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult DeleteRelationshipDegree(int? id)
        {
            string message = "";
            if (id != null)
            {
                RelationshipDegree relationshipDegree = _db.RelationshipDegrees.Find(id);
                if (relationshipDegree != null)
                {
                    try
                    {
                        _db.RelationshipDegrees.Remove(relationshipDegree);
                        _db.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        message = "Невозможно удалить! Данный пол используется.";
                    }
                }
            }
            return RedirectToAction("AllRelationshipDegrees", new { message });
        }

        //справочник шаблонов на работу
        [Authorize(Roles = "user")]
        [HttpGet]
        public IActionResult AllTemplateForWorks(string message)
        {
            var templates = _db.TemplateForWorks.ToList();
            ViewBag.Message = message;
            return View(templates);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult AddTemplateForWork()
        {
            return View();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult AddTemplateForWork(TemplateForWork templateForWork)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.TemplateForWorks.Add(templateForWork);
                    _db.SaveChanges();
                    message = "Запись добавлена!";
                    color = "#006400";
                }
                catch (DbUpdateException)
                {
                    message = "Данный пол уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message wm = new Message(message, "AllTemplateForWorks", "Directory", color);
            return PartialView("../_ViewMessage", wm);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult EditTemplateForWork(int? id)
        {
            if (id != null)
            {
                TemplateForWork templateForWork = _db.TemplateForWorks.Find(id);
                if (templateForWork != null)
                {
                    return View(templateForWork);
                }
            }
            return NotFound();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult EditTemplateForWork(TemplateForWork templateForWork)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.TemplateForWorks.Update(templateForWork);
                    _db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    message = "Данный пол уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message m = new Message(message, "AllTemplateForWorks", "Directory", color);
            return PartialView("../_ViewMessage", m);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult DeleteTemplateForWork(int? id)
        {
            string message = "";
            if (id != null)
            {
                TemplateForWork templateForWork = _db.TemplateForWorks.Find(id);
                if (templateForWork != null)
                {
                    try
                    {
                        _db.TemplateForWorks.Remove(templateForWork);
                        _db.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        message = "Невозможно удалить! Данный пол используется.";
                    }
                }
            }
            return RedirectToAction("AllTemplateForWorks", new { message });
        }
        
        //справочник вредности условий труда
        [Authorize(Roles = "user")]
        [HttpGet]
        public IActionResult AllHarms(string message)
        {
            var harms = _db.Harms.ToList();
            ViewBag.Message = message;
            return View(harms);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult AddHarm()
        {
            return View();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult AddHarm(Harm harm)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.Harms.Add(harm);
                    _db.SaveChanges();
                    message = "Запись добавлена!";
                    color = "#006400";
                }
                catch (DbUpdateException)
                {
                    message = "Данный пол уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message wm = new Message(message, "AllHarms", "Directory", color);
            return PartialView("../_ViewMessage", wm);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult EditHarm(int? id)
        {
            if (id != null)
            {
                Harm harm = _db.Harms.Find(id);
                if (harm != null)
                {
                    return View(harm);
                }
            }
            return NotFound();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult EditHarm(Harm harm)
        {
            string message = "";
            string color = "";

            if (ModelState.IsValid)
            {
                try
                {
                    _db.Harms.Update(harm);
                    _db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    message = "Данный пол уже содержится в базе!";
                }
            }
            else
            {
                message = "Произошла ошибка!";
            }
            Message m = new Message(message, "AllHarms", "Directory", color);
            return PartialView("../_ViewMessage", m);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult DeleteHarm(int? id)
        {
            string message = "";
            if (id != null)
            {
                Harm harm = _db.Harms.Find(id);
                if (harm != null)
                {
                    try
                    {
                        _db.Harms.Remove(harm);
                        _db.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        message = "Невозможно удалить! Данный пол используется.";
                    }
                }
            }
            return RedirectToAction("AllHarms", new { message });
        }

        //Документы
        [Authorize(Roles = "user")]
        [HttpGet]
        public IActionResult AllDocuments(string message)
        {
            var docs = _db.Documents.ToList();
            ViewBag.Message = message;
            return View(docs);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult AddDocument()
        {
            return View();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult AddDocument([FromForm] IFormFile file, [FromForm] string number_doc, [FromForm] DateTime date_doc)
        {
            string message = "";
            string color = "";

            if(file != null)
            {
                string savePath = Path.Combine(_env.WebRootPath, "Examination", file.FileName);
                using (FileStream fileStream = new FileStream(savePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }

                Document doc = new Document();

                doc.link = savePath;
                doc.number_document = number_doc;
                doc.Date = _db.Dates.FirstOrDefault(x => x.date == date_doc);

                _db.Documents.Add(doc);
                _db.SaveChanges();

                message = "Документ успешно загружен";
                color = "#006400";
            }
            else
            {
                message = "Прикрепите документ!";
            }
            
            Message m = new Message(message, "AllDocuments", "Directory", color);
            return PartialView("../_ViewMessage", m);
        }

        //Сотрудники
        [Authorize(Roles = "user")]
        [HttpGet]
        public IActionResult AllEmployees(string message)
        {
            List<Employee> emp = _db.Employees.ToList();
            ViewBag.Message = message;
            return View(emp);
        }

        //Люди
        [Authorize(Roles = "user")]
        [HttpGet]
        public IActionResult AllPersons(string message)
        {
            List<Person> pers = _db.Persons.Where(x => x.code == 3639).ToList();
            ViewBag.Message = message;
            return View(pers);
        }

        //Прейскурант БЕСИ
        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult AllBesiPrice()
        {
            var productTypes = _db.Besi_Type_Products.ToList();
            return View(productTypes);
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult AllBesiPrice(int? id)
        {
            var besi_Price_Lists = _db.Besi_Price_Lists.Where(x => x.code_p000091 == id).ToList();
            return PartialView("_PriceList", besi_Price_Lists);
        }

        //загрузить прайс лист
        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult UploadPriceList()
        {
            return View();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult UploadPriceList([FromForm] IFormFile file, DateTime date_doc)
        {
            string message = "";
            string color = "";

            if (file != null)
            {
                string savePath = Path.Combine(_env.WebRootPath, "Documents/PriceLists", file.FileName);
                using (FileStream fileStream = new FileStream(savePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }

                ParserOT parser = new ParserOT(_db);
                parser.ParserBesi(savePath, date_doc);

                Document doc = new Document();

                doc.link = savePath;
                doc.Date = _db.Dates.FirstOrDefault(x => x.date == date_doc);

                _db.Documents.Add(doc);
                _db.SaveChanges();

                message = "Документ успешно загружен";
                color = "#006400";
            }
            else
            {
                message = "Прикрепите документ!";
            }

            Message m = new Message(message, "AllDocuments", "Directory", color);
            return PartialView("../_ViewMessage", m);
        }

        [Authorize(Roles = "directory manager")]
        [HttpGet]
        public IActionResult EditPhotoBesi(int? id)
        {
            if (id != null)
            {
                Document doc = _db.Documents.Find(id);
                if (doc != null)
                {
                    return View(doc);
                }
            }
            return NotFound();
        }

        [Authorize(Roles = "directory manager")]
        [HttpPost]
        public IActionResult EditPhotoBesi([FromForm] IFormFile file, int id)
        {
            string message = "";
            string color = "";

            if (file != null)
            {
                string savePath = Path.Combine(_env.WebRootPath, "Documents/PriceLists/PhotosDrawings", file.FileName);
                using (FileStream fileStream = new FileStream(savePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }

                Document doc = _db.Documents.Find(id);

                doc.link = savePath;

                _db.Documents.Update(doc);
                _db.SaveChanges();

                message = "Документ успешно изменён";
                color = "#006400";
            }
            else
            {
                message = "Прикрепите документ!";
            }

            Message m = new Message(message, "AllDocuments", "Directory", color);
            return PartialView("../_ViewMessage", m);
        }
    }
}