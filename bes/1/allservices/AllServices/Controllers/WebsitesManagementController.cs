﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ModelsLibrary;
using AllServices.Models.WebsitesManagement;
using ModelsLibrary.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
/// <summary>
/// Author: Голубев Константин Игоревич
/// Creation date: 29.10.2019
/// </summary>
namespace AllServices.Controllers
{
    /// <summary>
    ///     Контроллер, описывающий методы для управления сайтами РУП "БЭС"
    /// </summary>
   
    public class WebsitesManagementController : Controller
    {
        private BesContext context;
        private static List<FilialModel> filialModels;
        private static int IdSelectedFilial { get; set; } = 0;

        public WebsitesManagementController(BesContext context)
        {
            this.context = context;
        }

        // Главная страница


        /// <summary>
        /// 
        ///     Управление сайтами РУП "БЭС"
        ///     
        /// </summary>
        /// <returns> Представление со списком разделов сайтов </returns>
        [Authorize(Roles = "review manager")]
        public IActionResult Index()
        {
            return View();
        }



        // Раздел отзывов


        /// <summary>
        /// 
        ///     Управление отзывами на сайтах
        ///     
        ///     Метод создаёт и инициализирует список объектов FilialModel для работы с отзывами для всех филиалов
        ///     
        /// </summary>
        /// <returns> Представление со списком всех филиалов </returns>

        [Authorize(Roles = "review manager")]
        public IActionResult Reviews()
        {
            filialModels = new List<FilialModel>();

            // Список организаций
            List<Organization> listOrg = new List<Organization>();

            for(int i = 9; i <= 17; i++)
            {
                // Объект FilialModel для филиала с идентификатором i
                FilialModel filialModel = new FilialModel();

                // В поле Organization объекта FilialModel помещается ссылка на объект Organization контекста
                filialModel.Organization = context.Organizations.Find(i);

                // Добавление в список организаций филиала с идентификатором i
                listOrg.Add(filialModel.Organization);

                // Список объектов всех отзывов для филиала с идентификатором i
                List<CompanyReview> companyReviews = context.CompanyReviews.Where(x => x.code_p000006 == i).ToList();

                // Список объектов ReviewsModel для отображения информации об отзыве (список отзывов)
                List<ReviewsModel> reviewsModels = new List<ReviewsModel>();

                // Инициализация объектов ReviewsModel для отображения информации об отзыве 
                // и добавление их в список reviewsModels
                foreach (CompanyReview item in companyReviews)
                {
                    ReviewsModel reviewsModel = new ReviewsModel();
                    reviewsModel.NameOrganization = item.CompanyThatMadeReview.name;
                    reviewsModel.DateReview = item.DateReview.date.ToShortDateString();
                    reviewsModel.ReviewText = item.review_text;
                    reviewsModel.Confirm = item.confirm;

                    reviewsModels.Add(reviewsModel);
                    
                }

                // В поле ReviewsModels объекта FilialModel помещается ссылка на 
                // инициализированный список объект ReviewsModel
                filialModel.ReviewsModels = reviewsModels;

                // Инициализированный объект FilialModel добавляется в список filialModels объектов FilialModel
                filialModels.Add(filialModel);
            }

            // Генерация выподающего списка SelectList для отображения аббревиатур филиалов РУП "БЭС"
            IEnumerable<Organization> list = (IEnumerable<Organization>)listOrg;
            ViewBag.selectList = new SelectList(list, "code", "abbreviation");

            return View();
        }


        /// <summary>
        /// 
        ///     Метод по значению входного параметра определяет, какой филиал был выбран для настройки
        ///     и передаёт в представление _ReviewList объект (FilialModel), 
        ///     содержащий информацию о филиале и обо всех оставленных для него отзывах.
        ///     
        ///     Если ни один филиал не выбран, отзывы не отображаются.
        /// 
        /// </summary>
        /// <param name="chackedFilial"> Код выбранного филиала </param>
        /// <returns> Представление со списком всех отзывов, оставленных для выбранного филиала </returns>

        [Authorize(Roles = "review manager")]
        [HttpPost]
        public IActionResult ReviewList([Bind("code")] Organization chackedFilial)
        {
            FilialModel filialModel = null;

            foreach(FilialModel filial in filialModels)
            {
                if (filial.Organization.code == chackedFilial.code)
                {
                    filialModel = filial;
                    IdSelectedFilial = chackedFilial.code;
                }
            }

            if(filialModel != null)
            {
                return PartialView("_ReviewList", filialModel);
            }

            return View("Reviews");
        }


        /// <summary>
        /// 
        ///     Метод отмечает выбранные для публикации на сайте отзывы
        ///     и вносит изменения в таблицу отзывов БД
        /// 
        /// </summary>
        /// <param name="model"> Объект, содержащий информацию о филиале и обо всех его отзывах </param>
        /// <returns> Перенаправление к методу Reviews </returns>
        [Authorize(Roles = "review manager")]
        [HttpPost]
        public IActionResult ReviewPublish(FilialModel model)
        {

            List<CompanyReview> reviews = null;

            // Получение списка отзывов для филиала
            reviews = context.CompanyReviews.Where(x => x.code_p000006 == IdSelectedFilial).ToList();
            
            if (reviews != null)
            {
                int i = 0;

                // Для каждого отзыва устанавливается метка о разрешении публикации на сайте
                foreach (ReviewsModel company in model.ReviewsModels)
                {
                    reviews[i].confirm = company.Confirm;

                    // Обновление содержимого списка CompanyReviews контекста
                    context.CompanyReviews.Update(reviews[i]);
                    i++;
                }

                // Сохранение изменений в БД
                context.SaveChanges();

            }            

            return RedirectToAction("Reviews");
        }



    }
}