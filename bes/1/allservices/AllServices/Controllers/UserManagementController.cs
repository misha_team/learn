﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AllServices.Areas.Identity.Data.Models;
using AllServices.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AllServices.Controllers
{
    [Authorize (Roles = "admin")]
    public class UserManagementController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<UserManagementController> _logger;

        public UserManagementController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, RoleManager<IdentityRole> roleManager, ILogger<UserManagementController> logger)
        {
            _userManager = userManager;
            _signManager = signInManager;
            _roleManager = roleManager;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View(_userManager.Users.ToList());
        }
        

        //Удаление пользователя
        //[HttpPost]
        public async Task<IActionResult> RemoveUser(string id)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (user != null)
            {

                IdentityResult result = await _userManager.DeleteAsync(user);
                
            }
            return RedirectToAction("Index");
        }

        //администрирование пользователя
        [HttpGet]
        public async Task<IActionResult> UserAdministration(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return RedirectToAction("Index");
            }
            UserAdministrationViewModel userAdmin = new UserAdministrationViewModel { Id = user.Id, Email = user.Email };

            return View(userAdmin);
        }

        //Сброс пароля
        [HttpPost]
        public async Task<IActionResult> ChangePassword(UserAdministrationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(model.Id);
                if(user != null)
                {
                    var _passwordValidator = 
                        HttpContext.RequestServices.GetService(typeof(IPasswordValidator<AppUser>)) as IPasswordValidator<AppUser>;
                    var _passwordHasher = 
                        HttpContext.RequestServices.GetService(typeof(IPasswordHasher<AppUser>)) as IPasswordHasher<AppUser>;
                    IdentityResult result =
                        await _passwordValidator.ValidateAsync(_userManager, user, model.NewPassword);
                    if (result.Succeeded)
                    {
                        user.PasswordHash = _passwordHasher.HashPassword(user, model.NewPassword);
                        await _userManager.UpdateAsync(user);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        foreach(var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }

                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Пользователь не найден");                
                }
            }

            return View(model);
        }


        [HttpGet]
        public async Task<IActionResult> GrantUser(string id)
        {
            if (id != null)
            {
                var user = await _userManager.FindByIdAsync(id);
                if (user != null)
                {
                    var userRoles = await _userManager.GetRolesAsync(user);
                    var Roles = _roleManager.Roles.Select(r => new CheckedRoles() { Role = r, Checked = false }).ToList();
                    var allRoles = _roleManager.Roles.ToList();
                    foreach (var userRole in userRoles)
                    {
                        var role = Roles.Find(r => r.Role.Name == userRole);
                        int index = Roles.IndexOf(role);
                        Roles[index].Checked = true;
                    }
                    GrantUserModel grantUser = new GrantUserModel() { User = user, CheckedRoles = Roles };
                    return View(grantUser);
                }
            }
            ModelState.AddModelError("", "Пользователь не найден!");
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> GrantUser([Bind(include: "User, CheckedRoles")]GrantUserModel model)
        {
            var user = await _userManager.FindByIdAsync(model.User.Id);
            if (user != null)
            {
                var rolesToAddFromModel = model.CheckedRoles.Where(r => r.Checked == true).Select(r => r.Role.Name);
                var allRoles = model.CheckedRoles.Select(r=>r.Role.Name).ToList();
                bool check;
                List<string> rolesToRemove = new List<string>();
                foreach (var role in allRoles)
                {
                    check = await _userManager.IsInRoleAsync(user, role);
                    if (check)
                    {
                        rolesToRemove.Add(role);
                    }
                }
                await _userManager.RemoveFromRolesAsync(user, rolesToRemove);
                check = false;
                List<string> rolesToAdd = new List<string>();
                foreach (var role in rolesToAddFromModel)
                {
                    check = await _roleManager.RoleExistsAsync(role);
                    if (check)
                    {
                        rolesToAdd.Add(role);
                    }
                }
                var result = await _userManager.AddToRolesAsync(user, rolesToAdd);
                if (result.Succeeded)
                {
                    _logger.LogToFileAndDatabase($"{user.Email} added to roles {rolesToAdd.ItemsToString()}");
                    return  RedirectToAction(nameof(Index));
                }
            }
            return RedirectToAction(nameof(Index));
        }

    }
}