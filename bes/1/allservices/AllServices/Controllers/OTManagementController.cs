﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using AllServices.Models.OTManagement;
using ModelsLibrary;
using ModelsLibrary.Models.OT;
using ModelsLibrary.Models;
using Microsoft.AspNetCore.Http;

using Microsoft.AspNetCore.Mvc.Rendering;

namespace AllServices.Controllers
{
    [Authorize(Roles = "ot manager")]
    public class OTManagementController : Controller
    {
        private static BesContext context;
        private readonly IHostingEnvironment env;

        public OTManagementController(BesContext _context, IHostingEnvironment env)
        {
            context = _context;
            this.env = env; 
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult CheckKnowledge()
        {

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Commissions()
        {
            List<Commission> commissions = context.Commissions.ToList();
            List<ModelCommissionCommand> model = new List<ModelCommissionCommand>();

            foreach (Commission commission in commissions)
            {
                ModelCommissionCommand modelCommand = new ModelCommissionCommand();
                modelCommand.IdCommand = commission.DocumentCommand.code;
                string numAndDate = commission.DocumentCommand.number_document + " от " + commission.DocumentCommand.Date.date.Day +"." + commission.DocumentCommand.Date.date.Month + "." + commission.DocumentCommand.Date.date.Year;
                modelCommand.NumAndDateCommand = numAndDate;

                model.Add(modelCommand);
            }

            IEnumerable<ModelCommissionCommand> list = (IEnumerable<ModelCommissionCommand>)model;
            ViewBag.selectList = new SelectList(list, "IdCommand", "NumAndDateCommand");

            return View();
        } 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="checkedCommission"></param>
        /// <returns></returns>
        public IActionResult CommissionList([Bind("IdCommand")] ModelCommissionCommand checkedCommission)
        {
            Commission commission = new Commission();
            Document document = context.Documents.Find(checkedCommission.IdCommand);
            commission = context.Commissions.Where(x => x.doc == document.code).First();


            return PartialView("_ShowMembersOfCommission", commission);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult AddCommissionForCheckKnowledge()
        {
            return View();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddCommissionForCheckKnowledge(ViewSetCommissionForCheckKnowledge model,[FromForm] IFormFile file)
        {
            string erroreMessage = "";
            if (model.Date.Year == 0001)
            {
                erroreMessage = "Выберите дату приказа!";
                ViewBag.Error = erroreMessage;
                return View();
            }
            if(model.NumCommand == 0)
            {
                erroreMessage = "Введите номер приказа!";
                ViewBag.Error = erroreMessage;
                return View();
            }

            Document document = new Document();
            document.number_document = model.NumCommand.ToString();
            DateTime date = new DateTime(model.Date.Year, model.Date.Month, model.Date.Day);
            Date dateCommand = context.Dates.Where(x => x.date == date).FirstOrDefault();
            document.Date = dateCommand;

            Commission commission = new Commission();

            if (file != null)
            {

                long dateticks = DateTime.Now.Ticks;
                long datemilliseconds = dateticks / 10000;


                string fileName = String.Format(@"Content\Files\OT\Commands\{0}_{1}", datemilliseconds, file.FileName);
                String savePath = Path.Combine(env.WebRootPath, fileName);

                using (FileStream fileStream = new FileStream(savePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }

                document.link = savePath;
            }

            commission.DocumentCommand = document;


            List<MemberOfCommission> memberOfCommissions = new List<MemberOfCommission>();

            Employee employee = new Employee();
            

            for (int i = 0; i < model.MembersOfCommission.Count; i++)
            {
                MemberOfCommission member = new MemberOfCommission();
                if (i == 0)
                {
                    employee = context.Employees.Where(x => x.code_p000011 == model.MembersOfCommission[i]).FirstOrDefault();
                    member.Employee = employee;
                    member.PostsOfCommission = context.PostsOfCommissions.Where(x => x.code_position == 1).FirstOrDefault();
                    memberOfCommissions.Add(member);
                    continue;
                }
                if (i == 1)
                {
                    employee = context.Employees.Where(x => x.code_p000011 == model.MembersOfCommission[i]).FirstOrDefault();
                    member.Employee = employee;
                    member.PostsOfCommission = context.PostsOfCommissions.Where(x => x.code_position == 2).FirstOrDefault();
                    memberOfCommissions.Add(member);
                    continue;
                }

                employee = context.Employees.Where(x => x.code_p000011 == model.MembersOfCommission[i]).FirstOrDefault();
                member.Employee = employee;
                member.PostsOfCommission = context.PostsOfCommissions.Where(x => x.code_position == 3).FirstOrDefault();
                memberOfCommissions.Add(member);

            }


            commission.MemberOfCommissions = memberOfCommissions;


            context.Commissions.Add(commission);
            context.SaveChanges();
            
            return PartialView("_CreateCommissionMessage");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult NewCommissionPartial(string searchText)
        {
            searchText = searchText.ToUpper();

            List<Person> foundPeople = new List<Person>();

            List<Person> people = context.Persons.ToList();

            string buffer;

            foreach (Person person in people)
            {
                buffer = (person.last_name + person.first_name + person.father_name).ToUpper();

                if (buffer.IndexOf(searchText) > -1 && person.Employee != null)
                {
                    foundPeople.Add(person);
                }
            }



            return PartialView("_NewCommissionPartial", foundPeople);
        }



    }
}