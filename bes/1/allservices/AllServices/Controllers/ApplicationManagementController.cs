﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace AllServices.Controllers
{
    [Authorize(Roles = "manager")]
    public class ApplicationManagementController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}