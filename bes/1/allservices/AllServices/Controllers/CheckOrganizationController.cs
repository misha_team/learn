﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using AllServices.Models.CheckOrganization;
using ModelsLibrary;
using ModelsLibrary.Models;
using Microsoft.Extensions.Logging;
using AllServices.Extensions;
using Microsoft.AspNetCore.Authorization;

/// <summary>
/// Author: Голубев Константин Игоревич
/// Creation date: 23.04.2019
/// </summary>
namespace AllServices.Controllers
{
    [Authorize(Roles = "user")]
    public class CheckOrganizationController : Controller
    {
        private readonly BesContext _db;
        ActionsOnOrgRisk ActionsOnOrgRisk { get; set; }
        private readonly ILogger _logger;

        public CheckOrganizationController(BesContext besContext, ILogger<CheckOrganizationController> logger)
        {
            _db = besContext;
            ActionsOnOrgRisk = new ActionsOnOrgRisk(besContext);
            _logger = logger;
        }

        
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        ///     Метод передаёт обекту ActionsOnOrgRisk значение введённого УНП для проверки.
        /// </summary>
        /// <param name="unn"> поле УНП проверяемой организации (объекта OrganizationRisk)</param>
        /// <returns>Частичное представление с уведомлением о результате проверки организации</returns>
        [HttpPost]
        public IActionResult Index([Bind("unn")] OrganizationRisk organizationRisk)
        {
            MessageColor mc = new MessageColor();
            if (ModelState.IsValid)
            {
                _logger.LogToFileAndDatabase($"{User.Identity.Name}, connection id: {HttpContext.TraceIdentifier} checked unn {organizationRisk.unn}");
                //  Проверка, есть ли организация в "черном" списке
                if (ActionsOnOrgRisk.CheckMatch(organizationRisk.unn))
                {
                    mc.message = "Данная организация есть в реестре с повышенным риском совершения нарушений в экономической сфере!";
                    mc.color = "#8B0000";
                    ViewBag.OrgInfo = _db.OrganizationRisks.Where(x => x.unn == organizationRisk.unn).First();
                }
                else
                {
                    mc.message = "Данная организация отсутствует в реестре юр. лиц с повышенным риском совершения нарушений в экономической сфере!";
                    mc.color = "#006400";
                }
            }
            return PartialView("WarningMessage", mc);

        }
        /// <summary>
        /// Метод вызывается в случае аварийной остановки приложения
        /// </summary>
        /// <returns>Представление с описанием ошибки</returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}